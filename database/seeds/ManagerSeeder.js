'use strict'
const Database = use('Database');
const Hash = use('Hash');
const moment = require('moment');

/*
|--------------------------------------------------------------------------
| ManagerSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class ManagerSeeder {
  async run () {
    await Database.table('managers')
      .insert({
        login: 'manager',
        email: 'admin@admin.com',
        password: await Hash.make('123qqq'),
        last_name: 'Admin',
        first_name: 'Admin',
        created_at: moment().utcOffset(0).unix(),
        updated_at: moment().utcOffset(0).unix()
      });
  }
}

module.exports = ManagerSeeder
