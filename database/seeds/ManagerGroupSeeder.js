'use strict'
const Database = use('Database');
/*
|--------------------------------------------------------------------------
| ManagerGroupSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class ManagerGroupSeeder {
  async run () {
    await Database.table('managers_groups')
      .insert({
        name: 'Administrator'
      });
  }
}

module.exports = ManagerGroupSeeder
