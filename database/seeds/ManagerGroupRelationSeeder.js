'use strict'
const Database = use('Database');
/*
|--------------------------------------------------------------------------
| ManagerGroupRelationSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class ManagerGroupRelationSeeder {
  async run () {
    await Database.table('managers_groups_relations')
      .insert({
        manager_id: 1,
        group_id: 1
      });
  }
}

module.exports = ManagerGroupRelationSeeder
