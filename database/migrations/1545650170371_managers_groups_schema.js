'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ManagersGroupsSchema extends Schema {
  up () {
    this.create('managers_groups', (table) => {
      table.increments()
      table.string('name');
    })
  }

  down () {
    this.drop('managers_groups')
  }
}

module.exports = ManagersGroupsSchema
