'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ManagersGroupsPermissionsRelationsSchema extends Schema {
  up () {
    this.create('managers_groups_permissions_relations', (table) => {
      table.increments()
      table.integer('group_id').index();
      table.string('permission_id').index();
    })
  }

  down () {
    this.drop('managers_groups_permissions_relations')
  }
}

module.exports = ManagersGroupsPermissionsRelationsSchema
