'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ManagersSchema extends Schema {
  up () {
    this.create('managers', (table) => {
      table.increments()
      table.string('login', 32).notNullable().unique()
      table.string('email', 254).notNullable().unique()
      table.string('password', 60).notNullable()
      table.string('remember_token', 100)
      table.string('last_name', 128)
      table.string('first_name', 128)
      table.integer('created_at')
      table.integer('updated_at')
      table.integer('deleted_at')
    })
  }

  down () {
    this.drop('managers')
  }
}

module.exports = ManagersSchema
