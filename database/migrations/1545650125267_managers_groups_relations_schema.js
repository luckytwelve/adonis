'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ManagersGroupsRelationsSchema extends Schema {
  up () {
    this.create('managers_groups_relations', (table) => {
      table.increments()
      table.integer('manager_id').index();
      table.integer('group_id').index();
    })
  }

  down () {
    this.drop('managers_groups_relations')
  }
}

module.exports = ManagersGroupsRelationsSchema
