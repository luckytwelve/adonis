module.exports = {
  apps : [{
    name        : "server",
    script      : "./server.js",
    watch       : true,
    ignore_watch : ["node_modules", "persist", "storage", ".git", ".idea", ".history", "public/js"],
    env: {
      "NODE_ENV": "development",
    },
    env_production : {
      "NODE_ENV": "production"
    }
  }]
}