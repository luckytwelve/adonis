#!/bin/bash
echo "Installing web dependencies..."

if [ ! -f "/app/.env" ]
then
    npm i
	cp /app/.env.example /app/.env
    adonis key:generate
    adonis migration:run
    adonis seed
fi

pm2 start ecosystem.config.js --watch

echo "Web entrypoint done!"

exec tail -f /dev/null