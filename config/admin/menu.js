'use strict'

const Route = use('Route')

module.exports =
  [
    {
      "index": 1,
      "text": "<span>Administrators<\/span><i class=\"icon-users\"><\/i>",
      "a.href": "#",
      "permissions": [
        "managers-view",
        "managers-groups-view"
      ],
      "childs": [
        {
          "index": 1,
          "text": "<span>Admin groups<\/span><i class=\"icon-collaboration\"><\/i>",
          "a.href": ()=>{return Route.url('admin.managers-groups.list')},
          "permissions": [
            "managers-groups-view"
          ],
          "childs": []
        },
        {
          "index": 2,
          "text": "<i class=\"icon-vcard\"><\/i><span>Admin list<\/span>",
          "a.href": ()=>{return Route.url('admin.managers.list')},
          "permissions": [
            "managers-view"
          ],
          "childs": []
        }
      ]
    }
  ];

