'use strict'

module.exports =
  [
    {
      'name': 'Admin groups',
      'children': [
        {
          'index': 1,
          'name': 'managers-groups-view',
          'description': 'Administrators group view'
        },
        {
          'index': 2,
          'name': 'managers-groups-save',
          'description': 'Administrators group save'
        },
        {
          'index': 3,
          'name': 'managers-groups-delete',
          'description': 'Administrators group delete'
        }
      ]
    },
    {
      'name': 'Admins',
      'children': [
        {
          'index': 1,
          'name': 'managers-view',
          'description': 'Administrators view'
        },
        {
          'index': 2,
          'name': 'managers-save',
          'description': 'Administrators save'
        },
        {
          'index': 3,
          'name': 'managers-delete',
          'description': 'Administrators delete'
        }
      ]
    }
  ];

