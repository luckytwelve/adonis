'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

class ManagerHxr {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle ({ request, response, auth, view }, next) {
    if(!request.ajax() && await auth.authenticator('manager').check()){
      return response.send(view.render('backend.index.index'))
    }
    await next()
  }
}

module.exports = ManagerHxr
