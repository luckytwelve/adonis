'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

class ManagerAuth {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle ({ request, response, auth }, next) {
    try {
      await auth.authenticator('manager').check()
    } catch (error) {
      if(request.ajax()){
        return response(401);
      } else {
        return response.redirect('/admin/account/login');
      }
    }
    await next();
  }
}

module.exports = ManagerAuth;
