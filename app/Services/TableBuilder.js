const { ServiceProvider } = require('@adonisjs/fold')

class TableBuilder extends ServiceProvider {
  register() {
    this.app.singleton('ARX/TableBuilder', () => {
      const View = this.app.use('Adonis/Src/View')
      return class TableBuilder {

        constructor( id ) {
          this.id = id;
          this.name = '';
          this.description = '';
          this.buttons = '';
          this.columns = '';
          this.view = 'backend._partials.datatable';
        }

        setName( name ) {
          this.name = name;
          return this;
        }

        setButtons( buttons = [] ) {
          this.buttons = buttons;
          return this;
        }

        setColums( columns = [] ){
          this.columns = columns;
          return this;
        }

        build() {
          return View.render(this.view, {
            id: this.id,
            name: this.name,
            description: this.description,
            columns: this.buildColumns(),
            buttons: this.buttons
          })
        }

        buildColumns( ){
          let columns = [];
          this.columns.forEach(( value )=>{
            let width = value['width'] ? `width="${value['width']}"` : ``;
            columns.push(`<th ${width}> ${value['title']} </th>`);
          });
          return columns.join('');
        }
      }
    });
  }
}

module.exports = TableBuilder;
