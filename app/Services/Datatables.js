const { ServiceProvider } = require('@adonisjs/fold')

class Datatables extends ServiceProvider {
  register () {
    this.app.singleton('ARX/Datatables', () => {
      const _ = require('lodash');
      return class Datatables {
        constructor(query, request) {
          this.query = query;
          this.request = request;
          this.draw = request.get('draw', 1);
          this.recordsTotal = 0;
          this.recordsFiltered = 0;
          this.filterParams = [];
          this.orderParams = [];
          this.paginateParams = {};
          this.additionalColumns = []; // { action: (add|edit|delete), column: string, callback: function }
        }

        parseRequest() {
          this.filterParams = this.parseFilter();
          this.orderParams = this.parseOrder();
          this.paginateParams = this.parsePaginate();
        }

        parseFilter() {
          let searchableColumns = {};
          const columns = this.getColumns();
          const dtColumns = this.request.get('columns', []);
          const searchValue = this.request.get('search.value', '');

          if(searchValue != '') {
            for(let key in dtColumns) {
              let column = dtColumns[key];

              if(column.searchable && columns.indexOf(column.name) != -1) {
                searchableColumns.push({
                  column: column.name,
                  value: searchValue
                });
              }
            }
          }

          return searchableColumns;
        }

        parseOrder() {
          let orderableColumns = [];
          const columns = this.getColumns();
          const dtOrder = this.request.input('order', []);
          const dtColumns = this.request.input('columns', []);

          if(dtOrder.length > 0) {
            for(let key in dtOrder) {
              let item = dtOrder[key];
              let column = dtColumns[item.column];

              if(column.orderable && columns.indexOf(column.name) != -1) {
                orderableColumns.push({
                  column: column.name,
                  sequence: item.dir
                });
              }
            }
          }

          return orderableColumns;
        }

        parsePaginate() {
          let paginateParams = {};
          const dtPaginate = this.request.only(['start', 'length']);

          if(dtPaginate.length && parseInt(dtPaginate.length) != -1) {
            paginateParams = {
              length: dtPaginate.length,
              start: dtPaginate.start
            };
          }

          return paginateParams;
        }

        getColumns() {
          let columns = [];
          let item = this.query._statements.find(function(item) {
            return item.grouping == 'columns';
          });

          if(item) {
            columns = item.value;
          }

          return columns;
        }

        addColumn(column, callback) {
          this.additionalColumns.push({
            action: 'add',
            column: column,
            callback: callback
          });

          return this;
        }

        editColumn(column, callback) {
          this.additionalColumns.push({
            action: 'edit',
            column: column,
            callback: callback
          });

          return this;
        }

        deleteColumn(column) {
          this.additionalColumns.push({
            action: 'delete',
            column: column
          });

          return this;
        }

        sortAdditionalColumns(columns) {
          let priority = {
            add: 1,
            edit: 2,
            delete: 3
          };

          columns = columns.map((column) => {
            column.action = priority[column.action];

            return column;
          });

          columns.sort((a, b) => {
            return a.action - b.action;
          });

          priority = _.invert(priority);

          columns = columns.map((column) => {
            column.action = priority[column.action];

            return column;
          });

          return columns;
        }

        additionalColumnsProcessing(results) {
          if(this.additionalColumns.length > 0) {
            // sort additional columns

            this.additionalColumns = this.sortAdditionalColumns(
              this.additionalColumns
            );

            for(let key in this.additionalColumns) {
              let additionalColumn = this.additionalColumns[key];

              switch(additionalColumn.action) {
                case 'add':
                  results = results.map((item) => {
                    if(item[additionalColumn.column] === undefined) {
                      item[additionalColumn.column] = additionalColumn.callback(item);
                    }

                    return item;
                  });
                  break;

                case 'edit':
                  results = results.map((item) => {
                    if(item[additionalColumn.column]) {
                      item[additionalColumn.column] = additionalColumn.callback(item);
                    }

                    return item;
                  });
                  break;

                case 'delete':
                  results = results.map((item) => {
                    if(item[additionalColumn.column]) {
                      delete item[additionalColumn.column];
                    }

                    return item;
                  });
                  break;
              }
            }
          }

          return results;
        }

        async getCount() {
          let query = this.query.clone();
          let result = await query.clearSelect().getCount();

          return result || 0;
        }

        async make() {
          this.recordsTotal = await this.getCount();

          this.parseRequest();

          if(this.filterParams.length > 0) {
            const filterParams = this.filterParams;
            this.query.where(function() {
              for( let key in filterParams ) {
                this.orWhere(filterParams[key].column, 'like', '%' + filterParams[key].value + '%')
              }
            });
          }

          this.recordsFiltered = await this.getCount();

          if(this.orderParams.length > 0) {
            const orderParams = this.orderParams;

            for( let key in orderParams ) {
              this.query.orderBy(orderParams[key].column, orderParams[key].sequence)
            }
          }

          if(this.paginateParams.length !== undefined && this.paginateParams.start !== undefined) {
            this.query.limit(parseInt(this.paginateParams.length)).offset(parseInt(this.paginateParams.start));
          }

          let results = await this.query;
          results = this.additionalColumnsProcessing(results);

          return {
            draw: parseInt(this.draw) + 1,
            data: results,
            recordsFiltered: parseInt(this.recordsFiltered),
            recordsTotal: parseInt(this.recordsTotal)
          };
        }
      }
    });
  }
}

module.exports = Datatables;
