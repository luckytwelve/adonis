const { ServiceProvider } = require('@adonisjs/fold')

class Menu extends ServiceProvider {
  register () {
    this.app.singleton('ARX/Menu', () => {
      return class Menu {
        constructor() {
          this.default = {
            index: 0,
            text: '',
            childs: null,
          }
        }

        static generateTag(tag, attributes) {
          const attr = [];

          for (let key in attributes) {
            if (['childs', 'text'].includes(key)) continue
            if (!key.startsWith(`${tag}.`)) continue;

            let attribute = attributes[key];

            if (typeof attributes['a.href'] === 'function') {
              attribute = attributes['a.href']();
            }

            attribute = Array.isArray(attribute) ? attribute.join(' ') : attribute;

            attr.push(`${key.replace(`${tag}.`, '')}="${attribute}"`)
          }

          return `<${tag} ${attr.join(' ')}>{content}</${tag}>`;
        }

        static build(menu) {
          const list = [];
          menu.sort(function (a, b) {
            a = a.index !== undefined ? a.index : 0;
            b = b.index !== undefined ? b.index : 0;
            return a > b;
          });

          menu.forEach((item) => {
            const node = this.normalize(item);
            const li = this.generateTag('li', node);
            const a = this.generateTag('a', node);
            let ul = '';

            if (Array.isArray(node.childs) && node.childs.length) {
              ul = this.generateTag('ul', node);
              let inner = this.build(node.childs);
              ul = ul.replace('{content}', inner);
            }
            list.push((li.replace('{content}', `${a}${ul}`)).replace('{content}', node.text));
          });
          return list.join('');
        }

        static normalize(node) {
          return {
            ...this.default,
            ...node
          };
        }
      }
    })
  }
}

module.exports = Menu;
