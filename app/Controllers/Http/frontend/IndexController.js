'use strict'

class IndexController {
  index ({ view }) {
    return view.render('frontend.index');
  }
}

module.exports = IndexController
