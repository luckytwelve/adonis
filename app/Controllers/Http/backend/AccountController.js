'use strict'
const { validate } = use('Validator')
const Notify = use('ARX/Notify');

class AccountController {
  login ({ view }) {
    return view.render('backend.account.login');
  }

  async loginJson({request, response, auth}){
    const { email, password } = request.all();
    const rules = {
      email: 'required|min:3|max:255',
      password: 'required|min:6|max:32'
    };
    const validation = await validate(request.all(), rules)
    if (validation.fails()) {
      return response.json(Notify.error(validation.messages()[0].message));
    }
    try{
      await auth.authenticator('manager').remember(true).attempt(email, password);
      return response.json(Notify.success('Validation success', {}));
    } catch (e){
      return response.json(Notify.error('Manager not found', {}));
    }
  }

  async logout({request, response, auth}){
    await auth.authenticator('manager').logout();
    return response.redirect('login');
  }
}

module.exports = AccountController;
