'use strict'

const Route = use('Route')
const View = use('View');
const Hash = use('Hash');
const moment = require('moment');
const Database = use('Database');
const TableBuilder = use('ARX/TableBuilder');
const Datatables = use('ARX/Datatables');
const Notify = use('ARX/Notify');
const Manager = use('App/Models/Manager');

class ManagersController {

  lists({ view }){
    let table = new TableBuilder('managers');
    table.setName('Administration list');
    table.setButtons([[`<a href="#${Route.url('admin.managers.edit')}" class="pull-right btn btn-success">Create</a>`]])
    table.setColums([
      {title: "#", width: '1%'},
      {title: "Login", width: '5%'},
      {title: "Email", width: '10%'},
      {title: "Last name", width: '15%'},
      {title: "First name", width: '15%'},
      {title: "Actions", width: '1%'}
    ]);
    return view.render('backend.managers.lists', {table: table.build()});
  }

  async listsJson({request, response}){
    const input = request.all();
    let search_idx = false;
    let search = Database
      .select('*')
      .from('managers')
      .pluck('managers.id');

    if(input.search.value !== '') {
      search = search.where((q) => {
        for (let key in input.columns) {
          if(!JSON.parse(input.columns[key].searchable)) continue;
          key == 0 ?
            q.whereRaw(`${input.columns[key].name}::text LIKE ?`, [`%${input.search.value}%`]) :
            q.orWhere(`${input.columns[key].name}`, 'LIKE', `%${input.search.value}%`)
        }
        return q;
      });
      search_idx = await search;
    }

    let query = Database
      .select('*')
      .from('managers');
    if(search_idx) {
      query = query.whereIn('managers.id', search_idx);
    }

    let table = new Datatables(query, request)
      .addColumn('actions', (row) => {
        let actions = [];
        actions.push(
          '<ul class="icons-list">' +
            '<li class="dropdown">' +
              '<a href="#" class="dropdown-toggle" data-toggle="dropdown">' +
                '<i class="icon-menu9"></i>' +
              '</a>' +
          '<ul class="dropdown-menu dropdown-menu-right">'
        );
        actions.push(`<li><a href="#${Route.url('admin.managers.edit')}?id=${row.id}"><i class="icon-cog4"></i> Edit</a></li>`);
        actions.push(`<li><a href="#${Route.url('admin.managers.delete')}?id=${row.id}" data-callback="managers::ReloadTable" data-confirm="Are you sure you want to delete the item with the ID ${row.id}?"> <i class="icon-trash"></i> Delete</a></li>`);
        actions.push('</ul></li></ul>');
        return actions.join('');
      });
    let res = await table.make();
    return response.json(res);
  }

  async edit({request, response}){
    const manager_id = request.only('id')['id'];
    let manager = {};
    if(manager_id) {
      manager = await Manager.find(parseInt(manager_id));
      if(!manager){
        return response.json(Notify.error('Manager not found', {}));
      }
    }
    return response.json({
      modal: {
        title: manager_id ? 'Edit admin' : 'Create new admin',
        content: View.render('backend.managers.form', {manager: manager}),
        cancel: true,
        submit: !manager,
        edit: !!manager,
      },
      success: true
    })

  }

  async save({request, response}){
    const input = request.all();
    if(input.id !== 'undefined'){
      let manager = await Manager.find(parseInt(input.id));
      manager.first_name = input.first_name;
      manager.last_name  = input.last_name;
      manager.login = input.login;
      manager.email = input.email;
      manager.created_at = moment().utcOffset(0).unix();
      manager.updated_at = moment().utcOffset(0).unix();
      if(input.password) manager.password = input.password;
      manager = await manager.save();
      if(manager){
        return response.json(Notify.success('Saved', {}));
      }
    } else {
      let manager = await Manager.create({
        login: input.login,
        email: input.email,
        password: input.password,
        first_name: input.first_name,
        last_name: input.last_name,
      });
      if(manager){
        return response.json(Notify.success('Saved', {}));
      }
    }
    return response.json(Notify.error('Something went wrong', {}));
  }

  async deleteManager({request, response}){
    const manager_id = request.only('id')['id'];
    let manager = await Manager.find(parseInt(manager_id));
    if(!manager){
      return response.json(Notify.error('Something went wrong. Manager not found', {}));
    }
    if(await manager.delete()){
      return response.json(Notify.success('Deleted', {}));
    }
  }
}

module.exports = ManagersController
