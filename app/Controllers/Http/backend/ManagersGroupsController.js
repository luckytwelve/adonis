'use strict'

const Route = use('Route')
const View = use('View');
const moment = require('moment');
const Database = use('Database');
const TableBuilder = use('ARX/TableBuilder');
const Datatables = use('ARX/Datatables');
const Notify = use('ARX/Notify');
const ManagerGroup = use('App/Models/ManagerGroup');

class ManagersGroupsController {

  lists ({view}) {
    let table = new TableBuilder('managers-groups');
    table.setName('Administration group list');
    table.setButtons([[`<a href="#${Route.url('admin.managers-groups.edit')}" class="pull-right btn btn-success">Create</a>`]])
    table.setColums([
      {title: "#", width: '1%'},
      {title: "Name", width: '90%'},
      {title: "Actions", width: '1%'}
    ]);
    return view.render('backend.managers-groups.lists', {table: table.build()});
  }

  async listsJson ({request, response}) {
    const input = request.all();
    let search_idx = false;
    let search = Database
      .select('*')
      .from('managers_groups')
      .pluck('managers_groups.id');

    if (input.search.value !== '') {
      search = search.where((q) => {
        for (let key in input.columns) {
          if (!JSON.parse(input.columns[key].searchable)) continue;
          key == 0 ?
            q.whereRaw(`${input.columns[key].name}::text LIKE ?`, [`%${input.search.value}%`]) :
            q.orWhere(`${input.columns[key].name}`, 'LIKE', `%${input.search.value}%`)
        }
        return q;
      });
      search_idx = await search;
    }

    let query = Database
      .select('*')
      .from('managers_groups');
    if (search_idx) {
      query = query.whereIn('managers_groups.id', search_idx);
    }

    let table = new Datatables(query, request)
      .addColumn('actions', (row) => {
        let actions = [];
        actions.push(
          '<ul class="icons-list">' +
          '<li class="dropdown">' +
          '<a href="#" class="dropdown-toggle" data-toggle="dropdown">' +
          '<i class="icon-menu9"></i>' +
          '</a>' +
          '<ul class="dropdown-menu dropdown-menu-right">'
        );
        actions.push(`<li><a href="#${Route.url('admin.managers-groups.edit')}?id=${row.id}"><i class="icon-cog4"></i> Edit</a></li>`);
        actions.push(`<li><a href="#${Route.url('admin.managers-groups.delete')}?id=${row.id}" data-callback="managers-groups::ReloadTable" data-confirm="Are you sure you want to delete the item with the ID ${row.id}?"> <i class="icon-trash"></i> Delete</a></li>`);
        actions.push('</ul></li></ul>');
        return actions.join('');
      });
    let res = await table.make();
    return response.json(res);
  }

  async edit({request, response}){
    const Config = use('Config')
    const permissions = Config.get('admin.permissions')
    const manager_group_id = request.only('id')['id'];
    let manager_group = {};
    let permission_list = [];

    if(manager_group_id) {
      let managers_groups_permissions_relations = await Database
        .select('*')
        .from('managers_groups_permissions_relations')
        .where('group_id', manager_group_id);
      managers_groups_permissions_relations.forEach(function(item, i, arr) {
        permission_list.push(item.permission_id);
      })

      manager_group = await ManagerGroup.find(manager_group_id);
      if(!manager_group){
        return response.json(Notify.error('Manager group not found', {}));
      }
    }

    return response.json({
      modal: {
        title: manager_group_id ? 'Edit admin group' : 'Create new admin group',
        content: View.render('backend.managers-groups.form', {manager_group: manager_group, permissions: permissions, permission_list: permission_list}),
        cancel: true,
        submit: !manager_group_id,
        edit: manager_group_id,
      },
      success: true
    })
  }

  async save({request, response}){
    const input = request.all();
    if(input.id === 'undefined'){
      if(parseInt(await ManagerGroup.query().where('name', input.name).getCount())) {
        return response.json(Notify.error('Group with this name already exists.', {}))
      }
      let manager_group = await ManagerGroup.create({
        name: input.name,
      });
      input.id = manager_group.id
    } else {
      if(await ManagerGroup.query().where('name', input.name).whereNot('id', input.id).getCount()) {
        return response.json(Notify.error('Group with this name already exists.', {}))
      }
      let manager_group = await ManagerGroup.find(parseInt(input.id));
      manager_group.name = input.name;
      await manager_group.save();

      await Database.table('managers_groups_permissions_relations').where('group_id', input.id).delete();
    }

    const permissions_ids = Object.keys(input.ManagerGroupsPermissionsList)
    let managers_groups_permissions_relations_insert = [];
    for(const permission_id of permissions_ids) {
      managers_groups_permissions_relations_insert.push({
        group_id: input.id,
        permission_id: permission_id,
      })
    }

    await Database
      .insert(managers_groups_permissions_relations_insert)
      .into('managers_groups_permissions_relations');

    return response.json(Notify.success('Saved', {}));
  }

  async delete({request, response}){
    const manager_group_id = request.only('id')['id'];
    if(parseInt(await Database.from('managers_groups_relations').where('group_id', manager_group_id).getCount())) {
      return response.json(Notify.error('Cannot delete. This group has managers', {}));
    }
    let manager_group = await ManagerGroup.find(parseInt(manager_group_id));
    if(!manager_group){
      return response.json(Notify.error('Something went wrong. Manager group not found', {}));
    }
    if(await manager_group.delete()){
      await Database.table('managers_groups_permissions_relations').where('group_id', manager_group_id).delete();
      return response.json(Notify.success('Deleted', {}));
    }
  }
}

module.exports = ManagersGroupsController
