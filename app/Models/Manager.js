'use strict'

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

/**
 *
 * @type {moment | ((inp?: moment.MomentInput, format?: moment.MomentFormatSpecification, strict?: boolean) => moment.Moment) | ((inp?: moment.MomentInput, format?: moment.MomentFormatSpecification, language?: string, strict?: boolean) => moment.Moment)}
 */
const moment = require('moment');

class Manager extends Model {
  static boot () {
    super.boot()

    /**
     * A hook to hash the manager password before saving
     * it to the database.
     */
    this.addHook('beforeSave', async (managerInstance) => {
      if (managerInstance.dirty.password) {
        managerInstance.password = await Hash.make(managerInstance.password)
      }
    })
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens () {
    return this.hasMany('App/Models/Token')
  }

  /**
   *
   * @param field
   * @param value
   * @returns {*|String}
   */
  static formatDates (field, value) {
    if (field === 'created_at' || field === 'updated_at') {
      if(typeof value === 'number'){
        return value
      } else {
        return moment().utcOffset(0).unix()
      }
    }
    return super.formatDates(field, value)
  }
}

module.exports = Manager
