up:
	docker-compose up

fresh:
	docker-compose stop
	docker rm adonis-app-server || true
	docker rm adonis-pg || true
	docker rmi adonis_web || true
	docker rmi adonis_pg || true
	docker-compose up

ssh-web:
	docker-compose exec web bash

ssh-pg:
	docker-compose exec pg bash