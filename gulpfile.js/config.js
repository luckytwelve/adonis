var Config = {
    site: {
        base_path: 'resources/assets/site',
        dest_path: 'public/assets'
    },
    getTaskSettings: function (task_name) {
        return task_name.split(':').reduce(function (result, key) {
            return result[key];
        }, this.tasks);
    },
    tasks: {
        site: {}
    }
};

Config.tasks.site = {
    css: {
        libs: {
            source: '/css/plugins/*.css',
            dist: '/css/'
        },
        sass: {
            source: '/css/sass/',
            dist: '/css/',
        },
        fonts: {
            source: '/css/fonts/**/*.{ttf,woff,woff2,eof,svg}',
            dist: '/css/fonts/',
        }
    },

    js: {
        libs: {
            source: '/js/libs/**/*.js',
            dist: '/js',
            order: [
                'jquery-2.2.4.min.js',
                '/**/*.js'
            ]
        },
        core: {
            source: '/js/core/**/*.js',
            dist: '/js',
            order: [
                'js/core/**/*',
                'js/core/app.js',
                'js/**/*',
            ]
        },
        modules: {
            source: '/js/modules/**/*.js',
            dist: '/js',
            order: [
                'js/core/**/*',
                'js/core/app.js',
                'js/**/*',
            ]
        }
    }
};

module.exports = Config;
