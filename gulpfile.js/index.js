var requireDir = require('./helpers/requireDir');
var path = require('path');
var gulp = require('gulp');

requireDir( path.join(__dirname, 'tasks') );

gulp.task('default', ['admin', 'site'], function() {});