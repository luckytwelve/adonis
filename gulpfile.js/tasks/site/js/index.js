var gulp = require('gulp');
var order = require('gulp-order');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var args = require('get-gulp-args')();
var Config = require('../../../config.js');

var BASE_PATH = Config.site.base_path;
var DEST_PATH = Config.site.dest_path;

gulp.task('site:js:libs', function() {
  var settings = Config.getTaskSettings('site:js:libs');
  var dest = gulp.src(BASE_PATH + settings.source)
    .pipe(order(settings.order));

  if (args.production != undefined) {
    dest = dest.pipe(uglify());
  }

  return dest.pipe(concat('libs.js'))
    .pipe(gulp.dest(DEST_PATH + settings.dist));
});

gulp.task('site:js:core', function() {
  var settings = Config.getTaskSettings('site:js:core');
  var dest = gulp.src(BASE_PATH + settings.source)
    .pipe(order(settings.order));

  if (args.production != undefined) {
    dest = dest.pipe(uglify());
  }

  return dest.pipe(concat('core.js'))
    .pipe(gulp.dest(DEST_PATH + settings.dist));
});

gulp.task('site:js:modules', function() {
  var settings = Config.getTaskSettings('site:js:modules');
  var dest = gulp.src(BASE_PATH + settings.source)
    .pipe(order(settings.order));

  if (args.production != undefined) {
    dest = dest.pipe(uglify());
  }

  return dest.pipe(concat('modules.js'))
    .pipe(gulp.dest(DEST_PATH + settings.dist));
});


gulp.task('site:js', [
  'site:js:libs',
  'site:js:core',
  'site:js:modules'
], function() {
  if (args.production == undefined) {
    var settings = Config.getTaskSettings('site:js:modules');
    gulp.watch(BASE_PATH + settings.source, ['site:js:modules']);
  }
});
