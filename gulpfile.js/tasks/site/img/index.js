var gulp = require('gulp');
var image = require('gulp-image');
var args = require('get-gulp-args')();
var Config = require('../../../config.js');

var BASE_PATH = Config.site.base_path;
var DEST_PATH = Config.site.dest_path;


gulp.task('site:img:copy', function (cb) {
    if (args.noimages === undefined) {
        gulp.src(BASE_PATH+'/img/**/**/**/*')
            .pipe(image({
                pngquant: true,
                optipng: false,
                zopflipng: true,
                jpegRecompress: false,
                jpegoptim: true,
                mozjpeg: true,
                guetzli: false,
                gifsicle: true,
                svgo: true,
                concurrent: 10
            }))
            .pipe(gulp.dest(DEST_PATH+'/img')).on('end', cb).on('error', cb);
    }
});

gulp.task('site:img', [
    'site:img:copy'
], function() {

});