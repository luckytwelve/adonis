var gulp = require('gulp');
var concat = require('gulp-concat');
var order = require('gulp-order');
var uglify = require('gulp-uglify');
var args = require('get-gulp-args')();
var cleanCSS = require('gulp-clean-css');
var rename = require('gulp-rename');
var less = require('gulp-less');
var browserify = require("browserify");
var babelify = require("babelify");
var source = require("vinyl-source-stream");
var fs = require('fs');

var COMPONENTS_BASE_PATH  = 'resources/assets/admin/libs/**/*';
var COMPONENTS_DEST_PATH  = 'public/assets/admin/js';
var ADM_BASE_PATH         = 'resources/assets/admin/adm';
var ADM_DEST_PATH         = 'public/assets/admin/js';
var ADM_MODULES_BASE_PATH = 'resources/assets/admin/adm/modules';
var ADM_MODULES_DEST_PATH = 'public/assets/admin/js';
var THEME_BASE_PATH       = 'resources/assets/admin/theme';
var THEME_DEST_PATH       = 'public/assets/admin';


gulp.task('admin:libs', function () {
  var buildOrder = [
    'jquery.min.js',
    'bootstrap.min.js',
    'blockui.min.js',
    'uniform.min.js',
    'moment.min.js',
    'picker.js',
    'picker.date.js',
    'picker.time.js',
    'theme.js',
    '**/*',
    '*',
  ];
  return gulp.src(COMPONENTS_BASE_PATH)
    .pipe(order(buildOrder))
    //.pipe(uglify())
    .pipe(concat('libs.min.js'))
    .pipe(gulp.dest(COMPONENTS_DEST_PATH));
});

gulp.task('admin:adm', function() {
  if (args.production === undefined) {
    gulp.watch(`${ADM_BASE_PATH}`, ['admin:adm']);
  }
  var b = browserify({
    extensions: [".jsx", ".js"],
    debug: false
  });

  var files = [
    `${ADM_BASE_PATH}/core/_ajax.js`,
    `${ADM_BASE_PATH}/core/_modal.js`,
    `${ADM_BASE_PATH}/core/_modules.js`,
    `${ADM_BASE_PATH}/core/_notifications.js`,
    `${ADM_BASE_PATH}/core/_template.js`,
    `${ADM_BASE_PATH}/core/_ui.js`,
    `${ADM_BASE_PATH}/core/adm.js`,
    `${ADM_BASE_PATH}/core/stylizer.js`
  ];

  b.add(files);

  return b.transform("babelify", {
    presets: [
      "es2015", "react", "stage-0", "stage-1", "stage-2", "stage-3"],
    plugins: [
      "syntax-decorators"
    ]
  })
    .bundle()
    .pipe(source('core.min.js'))
    .pipe(gulp.dest(ADM_DEST_PATH));
});

gulp.task('admin:modules', function () {
  if (args.production === undefined) {
    gulp.watch(ADM_MODULES_BASE_PATH + '/*.js', ['admin:modules']);
  }

  var b = browserify({
    extensions: [".jsx", ".js"],
    debug: false
  });

  fs.readdirSync(ADM_MODULES_BASE_PATH).filter((file) => {
    return (file.slice(-3) === '.js');
  }).forEach((file) => {
    b.add(`${ADM_MODULES_BASE_PATH}/${file}`);
  });

  return b.transform("babelify", {
      presets: [
        "es2015", "react", "stage-0", "stage-1", "stage-2", "stage-3"],
      plugins: [
        "syntax-decorators"
      ]
    })
    .bundle()
    .pipe(source('modules.min.js'))
    .pipe(gulp.dest(ADM_MODULES_DEST_PATH));
});

gulp.task('admin:theme:css:extras', function() {
  return gulp.src(THEME_BASE_PATH + '/css/extras/**/*.css')
    .pipe(concat('extras.min.css'))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest(THEME_DEST_PATH + '/css'));
});

gulp.task('admin:theme:css:less', function() {
  return gulp.src(THEME_BASE_PATH + '/css/less/*.less')
    .pipe(less())
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(rename({suffix: ".min"}))
    .pipe(gulp.dest(THEME_DEST_PATH + '/css'));
});

gulp.task('admin:theme:css', [
  'admin:theme:css:extras',
  'admin:theme:css:less'
], function() {
  if (args.production === undefined) {
    gulp.watch(THEME_BASE_PATH + '/css/**/**/**/**/**/**/*', ['admin:theme:css']);
  }
});

gulp.task('admin:theme:js', function() {
  var buildOrder = [];
  if (args.production === undefined) {
    gulp.watch(THEME_BASE_PATH + '/js/**/**/**/*', ['admin:theme:js']);
  }
  return gulp.src(THEME_BASE_PATH + '/js/**/**/**/*')
    .pipe(order(buildOrder))
    .pipe(uglify())
    .pipe(concat('      '))
    .pipe(gulp.dest(THEME_DEST_PATH + '/js'));
});

gulp.task('admin:theme', [
 // 'admin:theme:js',
  'admin:theme:css'
]);

gulp.task('admin', [
	'admin:theme',
	'admin:libs',
	'admin:adm',
	'admin:modules'
]);