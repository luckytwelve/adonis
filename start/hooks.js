const { hooks } = require('@adonisjs/ignitor');

hooks.after.providersBooted(() => {
  const View = use('View')
  const Config = use('Config')
  const menu = Config.get('admin.menu');
  const Menu = use('ARX/Menu');
  View.global('Menu', function () {
    return Menu.build(menu);
  })
})
