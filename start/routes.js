'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', 'frontend/IndexController.index');

Route.group(() => {
  /*
  |--------------------------------------------------------------------------
  | account
  |--------------------------------------------------------------------------
  |
  */
  Route
    .get('account/login', 'backend/AccountController.login')
    .as('account.login')
    .middleware('managerGuest')
  Route
    .post('account/login', 'backend/AccountController.loginJson')
    .as('account.login.json')
    .middleware('managerGuest')
  Route
    .get('account/logout', 'backend/AccountController.logout')
    .as('account.logout')
  /*
    |--------------------------------------------------------------------------
    | Dashboard
    |--------------------------------------------------------------------------
    |
    */
  Route
    .any('/', 'backend/DashboardController.index')
    .middleware(['managerXHR','managerAuth'])

  /*
    |--------------------------------------------------------------------------
    | Managers
    |--------------------------------------------------------------------------
    |
    */
  Route
    .any('managers/lists', 'backend/ManagersController.lists')
    .as('admin.managers.list')
    .middleware(['managerXHR', 'managerAuth'])
  Route
    .post('managers/listsJson', 'backend/ManagersController.listsJson')
    .as('admin.managers.listJson')
    .middleware(['managerXHR', 'managerAuth'])
  Route
    .post('managers/delete', 'backend/ManagersController.deleteManager')
    .as('admin.managers.delete')
    .middleware(['managerXHR', 'managerAuth'])
  Route
    .post('managers/edit', 'backend/ManagersController.edit')
    .as('admin.managers.edit')
    .middleware(['managerXHR', 'managerAuth'])
  Route
    .post('managers/save', 'backend/ManagersController.save')
    .as('admin.managers.save')
    .middleware(['managerXHR', 'managerAuth'])

  /*
    |--------------------------------------------------------------------------
    | Managers Groups
    |--------------------------------------------------------------------------
    |
    */
  Route
    .any('managers-groups/lists', 'backend/ManagersGroupsController.lists')
    .as('admin.managers-groups.list')
    .middleware(['managerXHR', 'managerAuth'])
  Route
    .post('managers-groups/listsJson', 'backend/ManagersGroupsController.listsJson')
    .as('admin.managers-groups.listJson')
    .middleware(['managerXHR', 'managerAuth'])
  Route
    .post('managers-groups/delete', 'backend/ManagersGroupsController.delete')
    .as('admin.managers-groups.delete')
    .middleware(['managerXHR', 'managerAuth'])
  Route
    .post('managers-groups/edit', 'backend/ManagersGroupsController.edit')
    .as('admin.managers-groups.edit')
    .middleware(['managerXHR', 'managerAuth'])
  Route
    .post('managers-groups/save', 'backend/ManagersGroupsController.save')
    .as('admin.managers-groups.save')
    .middleware(['managerXHR', 'managerAuth'])




}).prefix('admin')
