# Adonis fullstack application

### Requirements

* [Docker](https://www.docker.com/)
* [Docker Compose](https://docs.docker.com/compose/)

### Git
**Important** `$ git checkout -b my-new-branch` from master to prevent changes on master branch `before` local deploy
**Important** Set `filemode` to `false` in `.git/config` in project folder to prevent git from tracking permissions before your first commit

### Environment

You are a master of your machine and development environment. There is `default` Docker setup which you can use to kickstart required servers and start developing:

    docker-compose up

It takes about 10 minutes to get fully provisioned machine when running `docker-compose up` for the first time.

### Start using

* [Home page](http://localhost:3333)
* [Admin panel](http://localhost:3333/admin/account/login)

### Port Forwarding

Docker provides abillity to forward port to host. If you already use certain port on host machine just change first parameter in `ports` section of `docker-compose.yml` file:

ports:
  - "`54322`:5432"
  - "`3333`:3333"
  
### Docker Commands

sudo docker ps
sudo docker exec -it adonislrx_web_1 bash

Stop a running container through SIGTERM
docker stop web

Stop a running container through SIGKILL
docker kill web

Create an overlay network and specify a subnet
docker network create --subnet 10.1.0.0/24
--gateway 10.1.0.1 -d overlay mynet

List the networks
docker network ls

List the running containers
docker ps

Delete all running and stopped containers
docker rm -f $(docker ps -aq)

Create a new bash process inside the container and connect
it to the terminal
docker exec -it web bash

Print the last 100 lines of a container’s logs
docker logs --tail 100 web