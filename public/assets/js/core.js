/*
    AJAX default settings
*/
$.ajaxSetup({
    statusCode: {
       403: function() {
           App.notification.error({text: "You are not have permissions for this action."});
       },
       404: function() {
           App.notification.error({text: "Page not found."});
       }
   },
   error: function() {
       App.notification.error({text: "Something was wrong."});
   }
});

_Ajax = function (params) {
    var _success = params.success;
    delete params.success;

    if(params.noPreloader === undefined && !params.noPreloader){
        $('.pre-loader').fadeIn();
    }
    params = $.extend(true, {}, {
        type: 'post',
        dataType: 'json',
        cache: false,
        url: '',
        error: function ( request ) {
            if(params.noPreloader === undefined && !params.noPreloader) {
                $('.pre-loader').fadeOut();
            }
            if(parseInt(request.status) == 401) {
                window.location.href = '/login';
            } else {
                App.RequestParse( request );
            }
        },
        success: function ( request, codeMessage, xhr) {

            if(params.noPreloader === undefined && !params.noPreloader){
                $('.pre-loader').stop( true, true).fadeOut();
            }
            App.RequestParse( request );
            if (_success) {
                if ( typeof _success === 'function' ){
                    _success( request );
                } else if ( _success.indexOf('::') ){
                    _tmp = _success.split('::');
                    _success = App.modules.get( _tmp[0] )[_tmp[1]];
                    _success( request,  App.modules.get( _tmp[0] ));
                }
            }

        }
    }, params);
    $.ajax(params);
};

var _Cookies = {
  get: function(name) {
    var matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
  },
  set: function(name, value, options) {
    options = options || {};
    
      var expires = options.expires;
    
      if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
      }
      if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
      }
    
      value = encodeURIComponent(value);
    
      var updatedCookie = name + "=" + value;
    
      for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
          updatedCookie += "=" + propValue;
        }
      }
    
      document.cookie = updatedCookie;
  },
  delete: function(name) {
    this.set(name, "", {
      expires: -1
    });
  }
};
var _Lang = {
    locale: "en",
    langs: {
        ru: {
            email: {
                required: "Поле обязательное",
                email: "Неправильный формат E-mail"
            },
            password: {
                required: "Поле обязательное",
                password_minlength: "Не менее 6 символов",
                equalTo: "Пароли не совпадают"
            },
            first_name: {
                required: "Поле обязательное",
                minlength: "Не менее 2 символов",
                lettersonly: "Неправильный формат имени"
            },
            last_name: {
                required: "Поле обязательное",
                minlength: "Не менее 2 символов",
                lettersonly: "Неправильный формат имени"
            },
            name: {
                required: "Поле обязательное",
                minlength: "Не менее 2 символов",
                lettersonly: "Неправильный формат имени"
            },
            phone: {
                required: "Поле обязательное",
                minlength: "Не менее 9 символов"
            },
            country: {
                required: "Поле обязательное"
            },
            region: {
                required: "Поле обязательное"
            },
            city: {
                required: "Поле обязательное"
            },
            locality: {
                required: "Поле обязательное"
            },
            apartment: {
                required: "Поле обязательное"
            },
            house:{
                required: "Поле обязательное"
            },
            street:{
                required: "Поле обязательное"
            },
            card: {
                required: "Поле обязательное"
            },
            data:{
                required: "Поле обязательное"
            },
            cvv: {
                required: "Поле обязательное"
            },
            text: {
                required: "Поле обязательное",
                minlength: "Не менее 50 символов"
            },
            data_processing: {
                required: "Поле обязательное",
            },
            subject: {
                required: "Поле обязательное",
                minlength: "Не менее 2 символов"
            },
        },
        en: {
            email: {
                required: "Required field",
                email: "Invalid E-mail Format"
            },
            password: {
                required: "Required field",
                password_minlength: 'At least 6 characters',
                equalTo: "Passwords do not match"
            },
            first_name: {
                required: "Required field",
                minlength: "At least 2 characters",
                lettersonly: "Wrong name format"
            },
            last_name: {
                required: "Required field",
                minlength: "At least 2 characters",
                lettersonly: "Wrong name format"
            },
            name: {
                required: "Required field",
                minlength: "At least 2 characters",
                lettersonly: "Wrong name format"
            },
            phone: {
                required: "Required field",
                minlength: "At least 9 characters"
            },
            country: {
                required: "Required field"
            },
            region: {
                required: "Required field"
            },
            city: {
                required: "Required field"
            },
            locality: {
                required: "Required field"
            },
            apartment: {
                required: "Required field"
            },
            house: {
                required: "Required field"
            },
            street: {
                required: "Required field"
            },
            card: {
                required: "Required field"
            },
            data:{
                required: "Required field"
            },
            cvv: {
                required: "Required field"
            },
            text: {
                required: "Required field",
                minlength: "At least 50 characters",
            },
            data_processing: {
                required: "Required field",
            },
            subject: {
                required: "Required field",
                minlength: "At least 2 characters"
            },
        }
    },

    get: function (name) {
        function index(obj,i) {return obj !== undefined ? obj[i] : {}}
        var text = name.split('.').reduce(index, this.langs[App.lang.locale]);
        return (text !== undefined) ? text : '';
    },
};
/**
*
* @type {{name: string, el: {}, _init: Function, init: Function, events: Function, _parseCommand: Function, _eventsDestroy: Function, _eventInit: Function, destroy: Function}}
* @private
*/

var _Module = {
    name:'',
    el: {},
    _init: function () {
        this.destroy();

        if (this.init) this.init();
        if (this.events) this._eventInit();

        this.el().attr('data-instance', this);
    },
    _parseCommand: function( command ){
        var command = command.split(':');
        var event = command[0];
        command.splice(0, 1);
        var selector = command.join(':');

        return {
          event: event,
          selector:selector
        };
    },
    _eventsDestroy: function(){
        var module = this;
        for(var key in this.events){
            command = this._parseCommand( key );

            $(document).off(
              command.event,
              '[data-module=' + module.name + '] ' + command.selector
            );
        }
    },
    _eventInit: function(){
        var module = this;

        for(var key in this.events){
            command = this._parseCommand( key );

            (function($,command, key, module){
                var _function = module.events[key] === 'function' ?
                  module.events[key] :
                  module[module.events[key]];

                $(document).on(
                  command.event,
                  '[data-module=' + module.name + '] ' + command.selector,
                  function(e){
                    var result = _function.apply(this, [ e, module ] ) ;

                    return result;
                });
            })(jQuery, command, key, module)
        }
    },
    init: function () { },
    events: function () { },
    destroy: function(){
        var module = this;

        for(var key in this.events){
            command = this._parseCommand( key );

            $(document).off(
              command.event,
              '[data-module=' + module.name + '] ' + command.selector
            );
        }
    }
};

var _Modules = {
    modules: {},
    set: function (name, module) {
        module.name = name;
        module.el = function(){
            return $('[data-module=' + name + ']');
        }
        this.modules[name] = $.extend({}, _Module, module);
    },
    get: function (name) {
        return this.modules[name];
    },
    init: function () {
        $("[data-module]").each(function(){
            var module = $(this).attr('data-module');
            var isInit = !$(this).attr('data-instance') &&
              App.modules.modules[module];

            if ( isInit ) {
                App.modules.get(module)._init();
            }
        });
    }
};

var _Notification = {
    defaults: {
        theme: 'default',
        life: 3000,
    },
    success: function (content, params) {
        params = params || {};
        params.content = content;
        params.theme = 'success';
        this.message(params);
    },
    info: function (content, params) {
        params = params || {};
        params.content = content;
        params.theme = 'default';
        this.message(params);
    },
    warning: function (content, params) {
        params = params || {};
        params.content = content;
        params.theme = 'warning';
        this.message(params);
    },
    error: function (content, params) {
        params = params || {};
        params.content = content;
        params.theme = 'error';
        this.message(params);
    },
    message: function (params) {
        params = params || {};
        var params = $.extend({}, this.defaults, params);

        params.theme = params.theme || 'default';

        switch( params.theme ){
            case 'error':
              params.theme = 'bg-danger';
              //$.jGrowl(params.content, params);
            break;

            case 'warning':
              params.theme = 'bg-warning';
              //$.jGrowl(params.content, params);
            break;

            case 'success':
              var $template = '<span class="tooltip_content">' +
                '<span class="prof">' + params.content + '</span></span>';

              //$.jGrowl($template, { sticky: true, theme: 'bg-white', position: 'bottom-right' });
            break;
        }
    }
};

var App = {
    ajax: _Ajax,
    modules: _Modules,
    notification: _Notification,
    cookies: _Cookies,
    lang: _Lang,
    RequestParse: function (response) {
        if(typeof response.success !== 'undefined') {
            if(response.message !== undefined) {
                if(response.success) {
                    App.notification.success(response.message);
                } else {
                    App.notification.error(response.message);
                }
            }
        }
        if (response.redirect) window.location = response.redirect;

        App.modules.init();
    },
    isOnPage: function (selector) {
        return ($(selector).length) ? $(selector) : false;
    },
    init: function() {
        this.modules.init();
        $(document).on('click', 'a.js-ajax', function (e) {
            var href = $(this).attr('href');
            if ( !href) return;
            if ( href === '#') return;
            if ( $(this).attr('target') ) return;
            e.preventDefault();
            App.ajax({
                url: href,
                success: $(this).data('callback'),
                statusCode: {
                    401: function() {

                    },
                    403: function() {

                    },
                    404: function() {

                    }
                }
            });
        });
        $(document).on('submit', 'form.js-ajax', function () {
            var form = $(this);
            action = form.attr('action');

            if (!action) return;
            if (action === '#') return;
            App.ajax({
                url: action,
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: $(this).data('callback'),
                statusCode: {
                    401: function() {

                    },
                    403: function() {

                    },
                    404: function() {

                    }
                }
            });
            return false;
        });
    },
    backendValidation: function($container, errors){
        if($container !== undefined && errors !== undefined){
            for (var name in errors) {
                var message = (typeof errors[name] == 'string') ? errors[name] : errors[name][0];
                var $el = $container.find('[name="' + name + '"]');
                $el.closest('label').addClass('error');
                message = '<b id="'+  name + '-error" class="error" style="display: block;">' + message + '</b>';
                $container.find("#"+name+'-error').length ? $el.next('.error').remove() : '';
                !$container.find("#"+name+'-error').length ? $el.after(message) : '';
            }
            $('.form-preloader').stop( true, true).fadeOut( 200 );
            $('.form-preloader').remove();
            setTimeout(function(){
                for (var name in errors) {
                    var $el = $container.find("#"+name+'-error');
                    $el.closest('label').removeClass('error');
                    $el.remove();
                }
            }, 1500);
        }
    },

}
