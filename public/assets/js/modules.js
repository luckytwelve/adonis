'use strict';
// fixed svg show
//-----------------------------------------------------------------------------
function fixedSvg() {
    var baseUrl = window.location.protocol + '//' + window.location.host + window.location.pathname + window.location.search;
    $('use').filter(function() {
        return ($(this).attr("xlink:href").indexOf("#") > -1);
    }).each(function() {
        $(this).attr("xlink:href", baseUrl + $(this).attr("xlink:href").split('/').slice(-1)[0]);
    });
}

fixedSvg();

// checking if element for page
//-----------------------------------------------------------------------------------
function isOnPage(selector) {
    return ($(selector).length) ? $(selector) : false;
}

$(document).on('opening', '.remodal', function () {
    $('.page-wrapper').addClass("blur");
});

$(document).ready(function(){
    $('input').each(function(){
        if($(this).val().length != 0){
            $(this).parent('.steps-label').addClass("focus-steps-input");
        }
    });
});

$('.js-steps-input').on('focus', function(){
    $(this).parent('.steps-label').addClass("focus-steps-input");
});

$('.js-steps-input').on('blur', function(){
    if($(this).val().length <= 0){
        $(this).parent('.steps-label').removeClass("focus-steps-input");
    }
});

$('.js-steps-textarea').on('focus', function(){
    $(this).parent('.steps-label').addClass("focus-steps-input");
    $(this).siblings('.steps-placeholder').css({opacity: 0});
});

$('.js-steps-textarea').on('blur', function(){
    if($(this).val().length <= 0){
        $(this).parent('.steps-label').removeClass("focus-steps-input");
        $(this).siblings('.steps-placeholder').css({opacity: 1});
    }
});

$('.mobile-button-menu').click(function(){
    if($(this).hasClass('mobile-button-menu-open')){
        $('.header-right').slideUp();
        $(this).removeClass("mobile-button-menu-open");
        setTimeout(function(){
            $('.header-right').removeClass("flex");
        },300);
    } else{
        $('.header-right').slideDown();
        $(this).addClass("mobile-button-menu-open");
        $('.header-right').addClass("flex");
    }
});

$(document).on('focus', '.js-steps-textarea', function(){
    $(this).parent('.steps-label').addClass("focus-steps-input");
    $(this).siblings('.steps-placeholder').css({opacity: 0});
});

$(document).on('blur', '.js-steps-textarea', function(){
    if($(this).val().length <= 0){
        $(this).parent('.steps-label').removeClass("focus-steps-input");
        $(this).siblings('.steps-placeholder').css({opacity: 1});
    }
});

$(document).on('click', '.mobile-button-menu-header', function(){
    if($(this).hasClass('footer-menu-open')){
        $(this).removeClass('footer-menu-open');
        $(this).siblings('.mobile-footer').stop().slideUp();
        $('footer, .page-body').removeClass('blur');
        $('body').css({overflow: 'auto'});
    } else{
        $(this).addClass('footer-menu-open');
        $(this).siblings('.mobile-footer').stop().slideDown();
        $('footer, .page-body').addClass('blur');
        $('body').css({overflow: 'hidden'});
    }
});

var $form = $('form');
$form.each(function () {

    var thisForm = $(this);
    thisForm.on('change', 'input, textarea', function (e) {
         e.preventDefault();
         if (thisForm.validate().checkForm()) {
             thisForm.find('button[type="submit"]').addClass('enable-btn');
         } else {
             thisForm.find('button[type="submit"]').removeClass('enable-btn');
         }
     });
});

$('.js-mobiscroll-wakeTime').mobiscroll().time({
    theme: 'allswell',
    display: 'center',
    steps: {
        minute: 5
    },
    headerText: 'Wake-time',
    timeFormat: 'hh-ii-A',
    defaultValue: mobiscroll.util.datetime.parseDate('yy-mm-dd-hh-ii-A', '1900-1-1-8-0-0-AM'),
    animate: 'flip',
    focusOnClose: false,
    onClose: function () {
        $(this).valid();
    }
});

$('.js-mobiscroll-bedTime').mobiscroll().time({
    theme: 'allswell',
    display: 'center',
    steps: {
        minute: 5
    },
    headerText: 'Bed-time',
    timeFormat: 'hh-ii-A',
    defaultValue: mobiscroll.util.datetime.parseDate('yy-mm-dd-hh-ii-A', '1900-1-1-22-0-0-PM'),
    animate: 'flip',
    focusOnClose: false,
    onClose: function () {
        $(this).valid();
    }
});

$('.js-mobiscroll-hour').mobiscroll().time({
    theme: 'allswell',
    display: 'center',
    timeFormat: 'hh',
    headerText: 'Time interval',
    defaultValue: new Date(new Date().setHours(2, 0, 0, 0)),
    animate: 'flip',
    focusOnClose: false,
    onClose: function () {
        $(this).valid();
    }
});

$('.js-mobiscroll-reminder').mobiscroll().time({
    theme: 'allswell',
    display: 'center',
    steps: {
        minute: 5
    },
    headerText: 'Reminder',
    timeFormat: 'hh-ii-A',
    defaultValue: mobiscroll.util.datetime.parseDate('yy-mm-dd-hh-ii-A', '1900-1-1-10-0-0-AM'),
    animate: 'flip',
    focusOnClose: true
});

var stopVideo = function (player) {
    var vidSrc = player.prop('src').replace('autoplay=1', 'autoplay=0');
    player.prop('src', vidSrc);
};

$(document).on('closed', '.modal-video', function () {
    var $current, player;
    $current = $(".modal-video");

    player = $current.find("iframe").get(0);
    stopVideo($(player));
});

$(document).on('opening', '.refer-modal', function(){
    $('body').css({position: 'fixed'});
});

$(document).on('closed', '.refer-modal', function(){
    $('body').css({position: 'relative'});
});

var phoneMask = {
    'alias': 'phone',
    showMaskOnFocus: true,
    showMaskOnHover: false
};

$(document).on('click', '.js-new-recipient', function(){
    $('[data-mask="phone"]').inputmask("phone", phoneMask);
});

$('[data-mask="phone"]').inputmask("phone", phoneMask);

$(document).on('click', '.js-steps-label-fullname', function(){
    $(this).css({display: 'none'});
    $('.js-steps-half-block').css({display: 'flex'});
    $('.js-steps-half-block').find('.steps-input[name="first_name"]').trigger("refresh").focus();
    $('.js-steps-half-block').find('.steps-input[name="last_name"]');
});

$(document).ready(function(){
    var $input = $('.js-steps-label-fullname').siblings('.js-steps-half-block').find('.steps-input');
    if($input.length != 0 && $input.val().length != 0){
        $('.js-steps-label-fullname').css({display: 'none'});
        $('.js-steps-half-block').css({display: 'flex'});
        $('.js-steps-half-block').find('.steps-input[name="name"]').trigger("refresh").focus();
        $('.js-steps-half-block').find('.steps-input[name="lastName"]');
    }
});

$(document).on('keypress change', 'input[name="name"], input[name="lastName"]', function(e){
    if (e.keyCode > 48 && e.keyCode < 57) {
        return false;
    }
});

$(document).on('keypress keydown', 'input, textarea', function(){
    // var $this = $(this);
    // setTimeout(function(){
    //     $this.parents('.js-validate').valid();
    // }, 100);
});
var urlImgLoad = '';
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader(),
            fileName = input.files[0].name;
        reader.onload = function (e) {
            urlImgLoad = e.target.result;
            $('.loaded-img').append('<div class="img-item"><img class="loaded-img" src="'+urlImgLoad+'" alt=""><div class="img-name-text">'+fileName+'</div><div class="remove js-remove-img"><img src="/assets/img/remove-icon.svg" alt=""></div></div>');
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$(document).on('change', '.js-submitfile', function(){
    readURL(this);
    $(this).closest('.load-img-block').addClass('is-active');
});
$(document).on('click', '.js-remove-img', function(e){
    e.preventDefault();
    $(this).closest('.load-img-block').removeClass('is-active');
    $(this).closest('.img-item').remove();
    $('.js-submitfile').val('');

});

function recaptchaInit() {
    $('.g-recaptcha').each(function(index, el) {
        var $recaptcha = $(this);
        var $field     = $recaptcha.closest('.el-form-field');
        var sitekey    = $(this).data('sitekey');

        grecaptcha.render(el, {
            'sitekey' : sitekey,
            callback : function() {
                recaptchaOnSuccess($field);
            }
        });
    });
};

function recaptchaOnSuccess($field) {
  $('.js-recaptcha-check').prop('checked', true);
  if ($('form').validate().checkForm()) {
    $('form').find('button[type="submit"]').addClass('enable-btn');
  }
};

// $('input, textarea').on('focus',function () {
//     setTimeout((function (el) {
//         var strLength = el.value.length;
//         var el = $(el);
//         return function () {
//             if (el.setSelectionRange !== undefined) {
//                 el.setSelectionRange(strLength, strLength);
//             } else {
//                 $(el).val(el.value);
//             }
//         }
//     }(this)), 5);
// });

$(window).scroll(function(){
    if ( $(window).scrollTop() >= 10 ){
        $('.header').addClass('header-scrolled');
    } else {
        $('.header').removeClass('header-scrolled');
    }
});

$(window).on('load', function(){
    if ( $(window).scrollTop() >= 10 ){
        $('.header').addClass('header-scrolled');
    } else {
        $('.header').removeClass('header-scrolled');
    }
});

$(document).on('focus', 'input, textarea', function(){
    moveCursorToEnd($(this));
});

function moveCursorToEnd(input) {
    var originalValue = input.val();
    input.val('');
    input.val(originalValue);
}

$(window).on('load', function(){
    if(isOnPage('.steps')){
        var scrollLeftPos = $('.steps ul .current').position().left;
        $('.steps ul').scrollLeft(scrollLeftPos - 20);
    }
});

$(window).on('load', function(){
    //$('.pre-loader').fadeOut();
});
// custom jQuery validation
//-----------------------------------------------------------------------------------
var validator = {
    init: function () {
        $('form').each(function () {
            var $form = $(this);
            var name = $form.attr('name');
            if (validator.validatorRules.hasOwnProperty(name) || $form.hasClass('js-validate')) {
                var rules = validator.validatorRules[name];
                $form.validate({
                    rules:          rules,
                    errorElement:    'b',
                    errorClass:      'error',
                    focusInvalid:    false,
                    focusCleanup:    false,
                    onkeyup:         false,
                    errorPlacement: function (error, element) {
                        validator.setError($(element), error);
                    },
                    highlight: function (element, errorClass, validClass) {
                       var $el = validator.defineElement($(element));
                       if ($(element).attr('name') == 'file'){
                           setTimeout(function () {
                               $(element).parents('label').find('b.error').addClass('file-error');
                               $(element).parents('.img-load').after($(element).parents('label').find('b.error'));
                           }, 100)
                       } else {
                           if ($el) $el.closest('label').removeClass(validClass).addClass(errorClass);
                       }
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        var $el = validator.defineElement($(element));
                        if ($el) $el.closest('label').removeClass(errorClass).addClass(validClass);
                    },
                    onfocusout: function(element) {
                        if(!$(element).hasClass("after-valid")){
                            var $el = validator.defineElement($(element));
                            $el.valid();
                        }
                    },
                    messages: validator.messages
                });
            }
        });
    },
    setError: function ($el, message) {
        $el = this.defineElement($el);
        if ($el) this.domWorker.error($el, message);
    },
    defineElement: function ($el) {
        return $el;
    },
    domWorker: {
        error: function ($el, message) {
            $el.closest('label').addClass('error');
            $el.after(message);
        }
    },
    messages: {
        'field_test': {
            required: 'This field is required.'
        }
    },
    validatorRules: {
        'form_test': {
            'field_test': {
                required: true
            }
        }
    }
};

validator.init();

// validate by data attribute
//-----------------------------------------------------------------------------------
(function(){
    // add to validate form class 'js-validate'
    // add to validate field data-valid="test"
    //-----------------------------------------------------------------------------------
    var rules = {
        'time': {
            required: true,
            messages: {
                required: 'This filed required'
            }
        },
        'phone': {
            required: true,
            minlength: 9,
            messages: {
                required: "Not a valid phone",
                minlength: 'Minimum length 9 characters'
            }
        },
        'name': {
            required: true,
            minlength: 2,
            messages: {
                required: "Not a valid name",
                minlength: 'Minimum length 2 characters'
            }
        },
        'firstName': {
            required: true,
            minlength: 2,
            messages: {
                required: "Not a valid name",
                minlength: 'Minimum length 2 characters'
            }
        },
        'lastName': {
            required: true,
            minlength: 2,
            messages: {
                required: "Not a valid name",
                minlength: 'Minimum length 2 characters'
            }
        },
        'name-last': {
            required: true,
            minlength: 2,
            messages: {
                required: "This field is required.",
                minlength: 'Must have at least 2 characters!'
            }
        },
        'message': {
            required: true,
            minlength: 10,
            messages: {
                required: "Not valid comment",
                minlength: 'Not valid comment'
            }
        },
        'message-vacancy': {
            required: true,
            minlength: 10,
            messages: {
                required: "некорректное сообщение",
                minlength: 'некорректное сообщение'
            }
        },
        'registrationEmail': {
            required: true,
            email: true,
            remote: {
                url: '/account/check',
                type: 'post',
                dataType:"json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    email: function() { return $('[name="email"]').val(); }
                }
            },
            messages: {
                required: "Not a valid e-mail",
                email: 'Not a valid e-mail',
                remote: 'This email already exist'
            }
        },
        'email': {
            required: true,
            email: true,
            messages: {
                required: "Not a valid e-mail",
                email: 'Not a valid e-mail'
            }
        },
        'inviteEmail': {
            email: true,
            messages: {
                email: 'Not a valid e-mail'
            }
        },
        'password': {
            required: true,
            minlength: 6,
            messages: {
                required: "This field is required.",
                minlength: 'Must have at least 6 characters!'
            }
        },
        'confirm-password': {
            required: true,
            minlength: 6,
            equalTo: "#password",
            messages: {
                required: "You didn't confirm the password",
                minlength: 'Must have at least 6 characters!',
                equalTo: 'Passwords must match'
            }
        },
        'account-password': {
            minlength: 6,
            messages: {
                minlength: 'Must have at least 6 characters!'
            }
        },
        'account-confirm-password': {

            minlength: 6,
            equalTo: "#password",
            messages: {
                minlength: 'Must have at least 6 characters!',
                equalTo: 'Passwords must match'
            }
        },
        'password-req': {
            required: false,
            minlength: 6,
            messages: {
                required: "This field is required.",
                minlength: 'Must have at least 6 characters!'
            }
        },
        'confirm-password-req': {
            required: false,
            minlength: 6,
            equalTo: "#password",
            messages: {
                required: "You didn't confirm the password",
                minlength: 'Must have at least 6 characters!',
                equalTo: 'Passwords must match'
            }
        },
        'type': {
            required: true,
            messages: {
                required: "This field is required."
            }
        },
        'file': {
            required: true,
            messages: {
                required: "Please select an image"
            }
        },
        'questionnaire': {
            required: true,
            messages: {
                required: "Please select an image"
            }
        },
        'subscribe': {
            require_from_group: [1, ".phone-group"],
            messages: {
                require_from_group: "Выберите специализацию"
            }
        },
        'card': {
            required: true,
            messages: {
                required: "Enter card number"
            }
        },
        'expDate': {
            expDate: true,
            required: true,
            messages: {
                expDate: "Invalid Date",
                required: "Enter Date"
            }
        },
        'cvsCode': {
            required: true,
            messages: {
                required: "Enter cvs code"
            }
        },
        'address': {
            required: true,
            messages: {
                required: "Please enter billing address"
            }
        },
        'zipCode': {
            required: true,
            messages: {
                required: "Please enter zip code"
            }
        },
        'state': {
            required: true,
            messages: {
                required: "Please enter state"
            }
        },
        'city': {
            required: true,
            messages: {
                required: "Please enter city"
            }
        },
        'condition': {
            required: true,
            messages: {
                required: "Enter medical condition"
            }
        },
        'medicine': {
            required: true,
            messages: {
                required: "Enter medicine"
            }
        },
        'lastCard': {
            required: true,
            messages: {
                required: "Enter last 4 number on credit card"
            },
            minlength: 4,
            maxlength: 4
        },
        'recaptcha': {
            required: true,
            messages: {
                required: 'Check the checkbox'
            }
        }
    };

    for (var ruleName in rules) {
        $('[data-valid=' + ruleName + ']').each(function(){
            $(this).rules('add', rules[ruleName]);
        });
    }
}());

var dateValid = false,
    dateCurrent = new Date(),
    monthCurrent = dateCurrent.getMonth() + 1,
    yearCurrent = String(dateCurrent.getFullYear());
yearCurrent = yearCurrent[2] + yearCurrent[3];

$(window).on('load focusout', function() {
    checkValidExpDate('.exp-date');
});


// $(document).on('keyup', '.exp-date', function(){
//     checkValidExpDate($(this));
// });

function checkValidExpDate(el) {
    if (isOnPage('.exp-date')) {
        var expDate = $(el).val().split('/');
        var regexp = /\•+\/\•+/;
        if(regexp.test($(el).val())){
          dateValid = true;
        } else{
          ((Number(expDate[1]) > Number(yearCurrent)) || ((Number(expDate[1]) == Number(yearCurrent)) && ((Number(expDate[0]) >= Number(monthCurrent))))) ? dateValid = true : dateValid = false;
        }
    }

}

// custom rules
//-----------------------------------------------------------------------------------
$.validator.addMethod("email", function(value) {
    if (value == '') return true;
    var regexp = /[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    return regexp.test(value);
});

$.validator.addMethod("letters", function(value, element) {
    return this.optional(element) || /^[^1-9!@#\$%\^&\*\(\)\[\]:;,.?=+_<>`~\\\/"]+$/i.test(value);
});

$.validator.addMethod("digits", function(value, element) {
    return this.optional(element) || /^(\+?\d+)?\s*(\(\d+\))?[\s-]*([\d-]*)$/i.test(value);
});

$.validator.addMethod("valueNotEquals", function(value, element, arg){
    return arg != value;
}, "Value must not equal arg.");

$.validator.addMethod("expDate", function (value) {
    if (value == '') return true;
    var regexp = /((0[1-9]|1[012])[- /.]([0-9]{2}))|\•+\/\•+/;
    if (dateValid && regexp.test(value))
        return true
    else
        return false;
});


$.validator.addMethod( "require_from_group", function( value, element, options ) {
    var $fields = $( options[ 1 ], element.form ),
        $fieldsFirst = $fields.eq( 0 ),
        validator = $fieldsFirst.data( "valid_req_grp" ) ? $fieldsFirst.data( "valid_req_grp" ) : $.extend( {}, this ),
        isValid = $fields.filter( function() {
            return validator.elementValue( this );
        } ).length >= options[ 0 ];

    // Store the cloned validator for future validation
    $fieldsFirst.data( "valid_req_grp", validator );

    // If element isn't being validated, run each require_from_group field's validation rules
    if ( !$( element ).data( "being_validated" ) ) {
        $fields.data( "being_validated", true );
        $fields.each( function() {
            validator.element( this );
        } );
        $fields.data( "being_validated", false );
    }
    return isValid;
}, $.validator.format( "Please fill at least {0} of these fields." ) );
App.modules.set('login', {
    events: {
        'submit: form[name="form-login"]': 'submitForm'
    },


    init: function() {

    },


    submitForm: function(e){
        e.preventDefault();
        var _this = $(this);
        App.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: _this.attr('action'),
            type: 'post',
            data: _this.serialize(),
            success: function(response){
                if (response.validatorErrors) {

                    App.backendValidation(
                        $('[data-module="login"]'),
                        response.validatorErrors
                    );
                }
            }
        });
    },
});