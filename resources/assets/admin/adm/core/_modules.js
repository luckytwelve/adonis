/**
*
* @type {{name: string, el: {}, _init: Function, init: Function, events: Function, _parseCommand: Function, _eventsDestroy: Function, _eventInit: Function, destroy: Function}}
* @private
*/

const _Module = require('./_module');

const _Modules = {
    modules: {},
    set: (name, module) => {
        module.name = name;
        module.el = () =>{
            return $('[data-module='+name+']');
        }
        _Modules.modules[name] = $.extend({}, _Module, module);  //_module.extend( module )
    },
    get: (name) => {
        return _Modules.modules[name];
    },
    init: () => {
        $("[data-module]").each( (key, el) =>{
            const modul = $(el).attr('data-module');
            if ( !$(el).attr('data-instance') && ADM.modules.modules[modul] ) {
                ADM.modules.get(modul)._init();
            }
        });
    }
};
module.exports = _Modules;
