const _Notifications = require('./_notifications');
const _Template = require('./_template');
const _Modules = require('./_modules');
const _Modal = require('./_modal');
const _Ajax = require('./_ajax');
const _UI = require('./_ui');
const ADM = {
    setting: {
        lang: 'ru',
    },
    Notifications: _Notifications,
    template: _Template,
    ajax: _Ajax,
    modules: _Modules,
    modal: _Modal,
    ui: _UI,
    //@TODO: Deprecated
    showViewport: (viewName) => {
        ADM.template.load(viewName, (response) => {
            const obj = $.extend({}, ADM.setting, response);
            $('#viewport').html(ADM.template.parse(response, obj));
        });
    },
    setViewPort: (html) => {
        $('#viewport').html(ADM.template.parse(response, ADM.setting));
    },

    RequestParse: (request) => {
        if (request.notification) ADM.Notifications.message(request.notification);
        if (request.responseText)    {
			$('#viewport').html(request.responseText);
            ADM.ui.init( $('#viewport') );
        }
        if (request.modal)   ADM.modal.show(request.modal);
        if (request.redirect) window.location = request.redirect;
        if (request.blank) window.open(request.blank);
        ADM.modules.init();
    },

    CKEditorsUpdate: () => {
        for(var instanceName in CKEDITOR.instances){
            CKEDITOR.instances[instanceName].updateElement();
        }
    },

    init: () => {
        ADM.ajax({
          url: location.href,
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
        });

        $(window).on('popstate', (event) => {
            ADM.ajax({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: location.href,
                success: $(this).attr('data-callback')
            });
        });

        window.onload = (e) => {
			const hash = location.hash;
			if(hash == '' || hash == '#') return;
            ADM.ajax({ url: hash.substring(1),headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }, });
        };

        $(document).on('click', 'a', function (e) {
            const confirmData = $(this).attr('data-confirm');

            if(confirmData) {
                let isConfirmed = confirm(confirmData);
                if(!isConfirmed) return false;
            }

            var href = $(this).attr('href');

            if ( !href) return;
            if ( $(this).data('toggle') == 'tab' ) return;
            if ( $(this).hasClass('no-ajax') ) return;
            if ( href === '#') return;
            if(href.match(/^javascript:void.*/g) && href.match(/^javascript:void.*/g).length) return;
            if ( $(this).attr('target') ) return;
            if ( $(this).closest('.cke_reset').length > 0 ) return;
            if ( $(this).closest('.cke_reset_all').length > 0 ) return;

            e.preventDefault();

            if(CKEDITOR != undefined) {
                for (let instance in CKEDITOR.instances) {
                    CKEDITOR.instances[instance].updateElement();
                }
            }

            if (href.indexOf('#') != 0) {
                history.pushState({}, $(this).attr('title'), href);
            } else {
                href = href.substring(1);
            }

            ADM.ajax({
                url: href,
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: $(this).attr('data-callback')
            });
        });

        $(document).on('submit', 'form', function () {
            var form = $(this);
            let action = form.attr('action');

            if (!action) return;
            if (action === '#') return;

            if(CKEDITOR != undefined) {
                for (let instance in CKEDITOR.instances) {
                    CKEDITOR.instances[instance].updateElement();
                }
            }

            ADM.ajax({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: action,
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: $(this).attr('data-callback')
            });
            return false;
        });
    }
};
ADM.ui.set('component_name', {
  init: function(context) {
    $(".styled, .multiselect-container input", context).uniform({
      radioClass: 'choice'
    });

    // File input
    $(".file-styled", context).uniform({
      wrapperClass: 'bg-blue',
      fileButtonHtml: '<i class="icon-file-plus"></i>'
    });

  }
});

ADM.ui.set('ckeditor', {
  init: (context) => {
    const editors = $(context).find('[data-ui="ckeditor"]');
    if (editors.length > 0) {
      $.each(editors, (index, editor) => {
        CKEDITOR.replace($(editor).attr('name'), {
          filebrowserBrowseUrl: '/fs/dialog.php?type=2&editor=ckeditor&fldr=',
          filebrowserUploadUrl: '/fs/dialog.php?type=2&editor=ckeditor&fldr=',
          filebrowserImageBrowseUrl: '/fs/dialog.php?type=1&editor=ckeditor&fldr='
        });
      });
    }
  }
});
window.ADM = ADM;
module.exports = ADM;
