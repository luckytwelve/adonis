const _Notifications = {
  defaults: {
    title: '',
    text: '',
    addclass: 'bg-primary',
    delay: 3000,
    type: 'info'
  },
  success: (params) => {
    params = params || {};
    params.addclass = 'bg-success';
    _Notifications.show(params);
  },
  info: (params) => {
    params = params || {};
    params.addclass = 'bg-info';
    _Notifications.show(params);
  },
  warning: (params) => {
    params = params || {};
    params.addclass = 'bg-warning';
    _Notifications.show(params);
  },
  error: (params) => {
    params = params || {};
    params.addclass = 'bg-danger';
    _Notifications.show(params);
  },
  show: (params) => {
    new PNotify(params);
  },
  message: (params) => {
    params = params || {};
    params = $.extend({}, _Notifications.defaults, params);
    _Notifications[params.type](params);
  }
};
module.exports = _Notifications;