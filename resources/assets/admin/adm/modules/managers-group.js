const ADM = require('./../core/adm');
ADM.modules.set('managers-groups', {
  tableSelector: '#managers-groups',
  events: {

  },

  ReloadTable: function( response, module){
    $(module.tableSelector).DataTable().ajax.reload(null, false);
  },
  AfterFormSend: function( response, module ){
    if ( !response.notification ) return;
    if (response.notification.type == 'success'){
      $('.modal').modal('hide');
      $(module.tableSelector).DataTable().ajax.reload(null, false);
    }
  },

  init: function () {
    this.table = $(this.tableSelector).DataTable({
      processing: true,
      serverSide: true,
      ajax: {
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: ADM.managerPath + '/managers-groups/listsJson',
        type: 'POST',
      },
      columns: [
        {data: 'id', name: 'managers_groups.id'},
        {data: 'name', name: 'managers_groups.name'},
        {data: 'actions', name: 'actions', orderable: false, searchable: false}

      ],

      initComplete: function () {
        $('.dataTables_length select').select2({
          minimumResultsForSearch: Infinity,
          width: 'auto'
        });
      },
      drawCallback: function () {
        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
      },
      preDrawCallback: function() {
        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
      }
    });
  }
});
