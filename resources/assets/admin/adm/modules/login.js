const ADM = require('./../core/adm');

ADM.modules.set('account_login', {
  ADMcontroller: 'login',
  events: {
    'submit: #loginForm': 'login',
  },

  init: () => {
    $( () => {
      // Style checkboxes and radios
      $('.styled').uniform();
    });
  },

  login: function (e, module) {
    e.preventDefault();
    $('button[type=submit]', this).prop('disabled', true);
    const _this = this;

    $.ajax({
      type: 'post',
      dataType: 'json',
      cache: false,
      url: ADM.managerPath + '/account/login',
      data: $(this).serialize(),
      success: (response)=>{
        $('button[type=submit]', _this).prop('disabled', false);
        switch (response.notification.type) {
          case 'success':
            setTimeout(document.location.href = ADM.managerPath, 1000);
            break;
          case 'error':
            ADM.Notifications.error({
              text: response.notification.text
            });
            break;
        }
      },
      error: ()=>{
        $('button[type=submit]',_this).prop('disabled', false);
      }
    });

    return false;
  }
});
