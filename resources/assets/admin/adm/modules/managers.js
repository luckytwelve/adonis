const ADM = require('./../core/adm');
ADM.modules.set('managers', {
  tableSelector: '#managers',
  events: {
    'click .checkAllGroups': 'checkAllGroups',
  },

  checkAllGroups: function(e) {
    e.preventDefault();
    $('.checkbox :checkbox', $(this)).each(function() {this.checked = true;});
    $.uniform.update();
  },
  ReloadTable: function( response, module){
    $(module.tableSelector).DataTable().ajax.reload(null, false);
  },
  AfterFormSend: function( response, module ){
    if ( !response.notification ) return;
    if (response.notification.type == 'success'){
      $('.modal').modal('hide');
      $(module.tableSelector).DataTable().ajax.reload(null, false);
    }
  },
  init: function () {
    this.table = $(this.tableSelector).DataTable({
      processing: true,
      serverSide: true,
      ajax: {
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: ADM.managerPath + '/managers/listsJson',
        type: 'POST',
      },
      columns: [
        {data: 'id', name: 'managers.id'},
        {data: 'login', name: 'managers.login'},
        {data: 'email', name: 'managers.email'},
        {data: 'last_name', name: 'managers.last_name'},
        {data: 'first_name', name: 'managers.first_name'},
        {data: 'actions', name: 'actions', orderable: false, searchable: false}

      ],

      initComplete: function () {
        $('.dataTables_length select').select2({
          minimumResultsForSearch: Infinity,
          width: 'auto'
        });
      },
      drawCallback: function () {
        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
      },
      preDrawCallback: function() {
        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
      }
    });
  }
});
