var App = {
    ajax: _Ajax,
    modules: _Modules,
    notification: _Notification,
    cookies: _Cookies,
    lang: _Lang,
    RequestParse: function (response) {
        if(typeof response.success !== 'undefined') {
            if(response.message !== undefined) {
                if(response.success) {
                    App.notification.success(response.message);
                } else {
                    App.notification.error(response.message);
                }
            }
        }
        if (response.redirect) window.location = response.redirect;

        App.modules.init();
    },
    isOnPage: function (selector) {
        return ($(selector).length) ? $(selector) : false;
    },
    init: function() {
        this.modules.init();
        $(document).on('click', 'a.js-ajax', function (e) {
            var href = $(this).attr('href');
            if ( !href) return;
            if ( href === '#') return;
            if ( $(this).attr('target') ) return;
            e.preventDefault();
            App.ajax({
                url: href,
                success: $(this).data('callback'),
                statusCode: {
                    401: function() {

                    },
                    403: function() {

                    },
                    404: function() {

                    }
                }
            });
        });
        $(document).on('submit', 'form.js-ajax', function () {
            var form = $(this);
            action = form.attr('action');

            if (!action) return;
            if (action === '#') return;
            App.ajax({
                url: action,
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: $(this).data('callback'),
                statusCode: {
                    401: function() {

                    },
                    403: function() {

                    },
                    404: function() {

                    }
                }
            });
            return false;
        });
    },
    backendValidation: function($container, errors){
        if($container !== undefined && errors !== undefined){
            for (var name in errors) {
                var message = (typeof errors[name] == 'string') ? errors[name] : errors[name][0];
                var $el = $container.find('[name="' + name + '"]');
                $el.closest('label').addClass('error');
                message = '<b id="'+  name + '-error" class="error" style="display: block;">' + message + '</b>';
                $container.find("#"+name+'-error').length ? $el.next('.error').remove() : '';
                !$container.find("#"+name+'-error').length ? $el.after(message) : '';
            }
            $('.form-preloader').stop( true, true).fadeOut( 200 );
            $('.form-preloader').remove();
            setTimeout(function(){
                for (var name in errors) {
                    var $el = $container.find("#"+name+'-error');
                    $el.closest('label').removeClass('error');
                    $el.remove();
                }
            }, 1500);
        }
    },

}
