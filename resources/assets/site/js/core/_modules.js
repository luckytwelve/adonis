/**
*
* @type {{name: string, el: {}, _init: Function, init: Function, events: Function, _parseCommand: Function, _eventsDestroy: Function, _eventInit: Function, destroy: Function}}
* @private
*/

var _Module = {
    name:'',
    el: {},
    _init: function () {
        this.destroy();

        if (this.init) this.init();
        if (this.events) this._eventInit();

        this.el().attr('data-instance', this);
    },
    _parseCommand: function( command ){
        var command = command.split(':');
        var event = command[0];
        command.splice(0, 1);
        var selector = command.join(':');

        return {
          event: event,
          selector:selector
        };
    },
    _eventsDestroy: function(){
        var module = this;
        for(var key in this.events){
            command = this._parseCommand( key );

            $(document).off(
              command.event,
              '[data-module=' + module.name + '] ' + command.selector
            );
        }
    },
    _eventInit: function(){
        var module = this;

        for(var key in this.events){
            command = this._parseCommand( key );

            (function($,command, key, module){
                var _function = module.events[key] === 'function' ?
                  module.events[key] :
                  module[module.events[key]];

                $(document).on(
                  command.event,
                  '[data-module=' + module.name + '] ' + command.selector,
                  function(e){
                    var result = _function.apply(this, [ e, module ] ) ;

                    return result;
                });
            })(jQuery, command, key, module)
        }
    },
    init: function () { },
    events: function () { },
    destroy: function(){
        var module = this;

        for(var key in this.events){
            command = this._parseCommand( key );

            $(document).off(
              command.event,
              '[data-module=' + module.name + '] ' + command.selector
            );
        }
    }
};

var _Modules = {
    modules: {},
    set: function (name, module) {
        module.name = name;
        module.el = function(){
            return $('[data-module=' + name + ']');
        }
        this.modules[name] = $.extend({}, _Module, module);
    },
    get: function (name) {
        return this.modules[name];
    },
    init: function () {
        $("[data-module]").each(function(){
            var module = $(this).attr('data-module');
            var isInit = !$(this).attr('data-instance') &&
              App.modules.modules[module];

            if ( isInit ) {
                App.modules.get(module)._init();
            }
        });
    }
};
