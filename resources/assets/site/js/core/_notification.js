var _Notification = {
    defaults: {
        theme: 'default',
        life: 3000,
    },
    success: function (content, params) {
        params = params || {};
        params.content = content;
        params.theme = 'success';
        this.message(params);
    },
    info: function (content, params) {
        params = params || {};
        params.content = content;
        params.theme = 'default';
        this.message(params);
    },
    warning: function (content, params) {
        params = params || {};
        params.content = content;
        params.theme = 'warning';
        this.message(params);
    },
    error: function (content, params) {
        params = params || {};
        params.content = content;
        params.theme = 'error';
        this.message(params);
    },
    message: function (params) {
        params = params || {};
        var params = $.extend({}, this.defaults, params);

        params.theme = params.theme || 'default';

        switch( params.theme ){
            case 'error':
              params.theme = 'bg-danger';
              //$.jGrowl(params.content, params);
            break;

            case 'warning':
              params.theme = 'bg-warning';
              //$.jGrowl(params.content, params);
            break;

            case 'success':
              var $template = '<span class="tooltip_content">' +
                '<span class="prof">' + params.content + '</span></span>';

              //$.jGrowl($template, { sticky: true, theme: 'bg-white', position: 'bottom-right' });
            break;
        }
    }
};
