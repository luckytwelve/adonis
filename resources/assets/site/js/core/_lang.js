var _Lang = {
    locale: "en",
    langs: {
        ru: {
            email: {
                required: "Поле обязательное",
                email: "Неправильный формат E-mail"
            },
            password: {
                required: "Поле обязательное",
                password_minlength: "Не менее 6 символов",
                equalTo: "Пароли не совпадают"
            },
            first_name: {
                required: "Поле обязательное",
                minlength: "Не менее 2 символов",
                lettersonly: "Неправильный формат имени"
            },
            last_name: {
                required: "Поле обязательное",
                minlength: "Не менее 2 символов",
                lettersonly: "Неправильный формат имени"
            },
            name: {
                required: "Поле обязательное",
                minlength: "Не менее 2 символов",
                lettersonly: "Неправильный формат имени"
            },
            phone: {
                required: "Поле обязательное",
                minlength: "Не менее 9 символов"
            },
            country: {
                required: "Поле обязательное"
            },
            region: {
                required: "Поле обязательное"
            },
            city: {
                required: "Поле обязательное"
            },
            locality: {
                required: "Поле обязательное"
            },
            apartment: {
                required: "Поле обязательное"
            },
            house:{
                required: "Поле обязательное"
            },
            street:{
                required: "Поле обязательное"
            },
            card: {
                required: "Поле обязательное"
            },
            data:{
                required: "Поле обязательное"
            },
            cvv: {
                required: "Поле обязательное"
            },
            text: {
                required: "Поле обязательное",
                minlength: "Не менее 50 символов"
            },
            data_processing: {
                required: "Поле обязательное",
            },
            subject: {
                required: "Поле обязательное",
                minlength: "Не менее 2 символов"
            },
        },
        en: {
            email: {
                required: "Required field",
                email: "Invalid E-mail Format"
            },
            password: {
                required: "Required field",
                password_minlength: 'At least 6 characters',
                equalTo: "Passwords do not match"
            },
            first_name: {
                required: "Required field",
                minlength: "At least 2 characters",
                lettersonly: "Wrong name format"
            },
            last_name: {
                required: "Required field",
                minlength: "At least 2 characters",
                lettersonly: "Wrong name format"
            },
            name: {
                required: "Required field",
                minlength: "At least 2 characters",
                lettersonly: "Wrong name format"
            },
            phone: {
                required: "Required field",
                minlength: "At least 9 characters"
            },
            country: {
                required: "Required field"
            },
            region: {
                required: "Required field"
            },
            city: {
                required: "Required field"
            },
            locality: {
                required: "Required field"
            },
            apartment: {
                required: "Required field"
            },
            house: {
                required: "Required field"
            },
            street: {
                required: "Required field"
            },
            card: {
                required: "Required field"
            },
            data:{
                required: "Required field"
            },
            cvv: {
                required: "Required field"
            },
            text: {
                required: "Required field",
                minlength: "At least 50 characters",
            },
            data_processing: {
                required: "Required field",
            },
            subject: {
                required: "Required field",
                minlength: "At least 2 characters"
            },
        }
    },

    get: function (name) {
        function index(obj,i) {return obj !== undefined ? obj[i] : {}}
        var text = name.split('.').reduce(index, this.langs[App.lang.locale]);
        return (text !== undefined) ? text : '';
    },
};