/*
    AJAX default settings
*/
$.ajaxSetup({
    statusCode: {
       403: function() {
           App.notification.error({text: "You are not have permissions for this action."});
       },
       404: function() {
           App.notification.error({text: "Page not found."});
       }
   },
   error: function() {
       App.notification.error({text: "Something was wrong."});
   }
});

_Ajax = function (params) {
    var _success = params.success;
    delete params.success;

    if(params.noPreloader === undefined && !params.noPreloader){
        $('.pre-loader').fadeIn();
    }
    params = $.extend(true, {}, {
        type: 'post',
        dataType: 'json',
        cache: false,
        url: '',
        error: function ( request ) {
            if(params.noPreloader === undefined && !params.noPreloader) {
                $('.pre-loader').fadeOut();
            }
            if(parseInt(request.status) == 401) {
                window.location.href = '/login';
            } else {
                App.RequestParse( request );
            }
        },
        success: function ( request, codeMessage, xhr) {

            if(params.noPreloader === undefined && !params.noPreloader){
                $('.pre-loader').stop( true, true).fadeOut();
            }
            App.RequestParse( request );
            if (_success) {
                if ( typeof _success === 'function' ){
                    _success( request );
                } else if ( _success.indexOf('::') ){
                    _tmp = _success.split('::');
                    _success = App.modules.get( _tmp[0] )[_tmp[1]];
                    _success( request,  App.modules.get( _tmp[0] ));
                }
            }

        }
    }, params);
    $.ajax(params);
};
