// custom jQuery validation
//-----------------------------------------------------------------------------------
var validator = {
    init: function () {
        $('form').each(function () {
            var $form = $(this);
            var name = $form.attr('name');
            if (validator.validatorRules.hasOwnProperty(name) || $form.hasClass('js-validate')) {
                var rules = validator.validatorRules[name];
                $form.validate({
                    rules:          rules,
                    errorElement:    'b',
                    errorClass:      'error',
                    focusInvalid:    false,
                    focusCleanup:    false,
                    onkeyup:         false,
                    errorPlacement: function (error, element) {
                        validator.setError($(element), error);
                    },
                    highlight: function (element, errorClass, validClass) {
                       var $el = validator.defineElement($(element));
                       if ($(element).attr('name') == 'file'){
                           setTimeout(function () {
                               $(element).parents('label').find('b.error').addClass('file-error');
                               $(element).parents('.img-load').after($(element).parents('label').find('b.error'));
                           }, 100)
                       } else {
                           if ($el) $el.closest('label').removeClass(validClass).addClass(errorClass);
                       }
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        var $el = validator.defineElement($(element));
                        if ($el) $el.closest('label').removeClass(errorClass).addClass(validClass);
                    },
                    onfocusout: function(element) {
                        if(!$(element).hasClass("after-valid")){
                            var $el = validator.defineElement($(element));
                            $el.valid();
                        }
                    },
                    messages: validator.messages
                });
            }
        });
    },
    setError: function ($el, message) {
        $el = this.defineElement($el);
        if ($el) this.domWorker.error($el, message);
    },
    defineElement: function ($el) {
        return $el;
    },
    domWorker: {
        error: function ($el, message) {
            $el.closest('label').addClass('error');
            $el.after(message);
        }
    },
    messages: {
        'field_test': {
            required: 'This field is required.'
        }
    },
    validatorRules: {
        'form_test': {
            'field_test': {
                required: true
            }
        }
    }
};

validator.init();

// validate by data attribute
//-----------------------------------------------------------------------------------
(function(){
    // add to validate form class 'js-validate'
    // add to validate field data-valid="test"
    //-----------------------------------------------------------------------------------
    var rules = {
        'time': {
            required: true,
            messages: {
                required: 'This filed required'
            }
        },
        'phone': {
            required: true,
            minlength: 9,
            messages: {
                required: "Not a valid phone",
                minlength: 'Minimum length 9 characters'
            }
        },
        'name': {
            required: true,
            minlength: 2,
            messages: {
                required: "Not a valid name",
                minlength: 'Minimum length 2 characters'
            }
        },
        'firstName': {
            required: true,
            minlength: 2,
            messages: {
                required: "Not a valid name",
                minlength: 'Minimum length 2 characters'
            }
        },
        'lastName': {
            required: true,
            minlength: 2,
            messages: {
                required: "Not a valid name",
                minlength: 'Minimum length 2 characters'
            }
        },
        'name-last': {
            required: true,
            minlength: 2,
            messages: {
                required: "This field is required.",
                minlength: 'Must have at least 2 characters!'
            }
        },
        'message': {
            required: true,
            minlength: 10,
            messages: {
                required: "Not valid comment",
                minlength: 'Not valid comment'
            }
        },
        'message-vacancy': {
            required: true,
            minlength: 10,
            messages: {
                required: "некорректное сообщение",
                minlength: 'некорректное сообщение'
            }
        },
        'registrationEmail': {
            required: true,
            email: true,
            remote: {
                url: '/account/check',
                type: 'post',
                dataType:"json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    email: function() { return $('[name="email"]').val(); }
                }
            },
            messages: {
                required: "Not a valid e-mail",
                email: 'Not a valid e-mail',
                remote: 'This email already exist'
            }
        },
        'email': {
            required: true,
            email: true,
            messages: {
                required: "Not a valid e-mail",
                email: 'Not a valid e-mail'
            }
        },
        'inviteEmail': {
            email: true,
            messages: {
                email: 'Not a valid e-mail'
            }
        },
        'password': {
            required: true,
            minlength: 6,
            messages: {
                required: "This field is required.",
                minlength: 'Must have at least 6 characters!'
            }
        },
        'confirm-password': {
            required: true,
            minlength: 6,
            equalTo: "#password",
            messages: {
                required: "You didn't confirm the password",
                minlength: 'Must have at least 6 characters!',
                equalTo: 'Passwords must match'
            }
        },
        'account-password': {
            minlength: 6,
            messages: {
                minlength: 'Must have at least 6 characters!'
            }
        },
        'account-confirm-password': {

            minlength: 6,
            equalTo: "#password",
            messages: {
                minlength: 'Must have at least 6 characters!',
                equalTo: 'Passwords must match'
            }
        },
        'password-req': {
            required: false,
            minlength: 6,
            messages: {
                required: "This field is required.",
                minlength: 'Must have at least 6 characters!'
            }
        },
        'confirm-password-req': {
            required: false,
            minlength: 6,
            equalTo: "#password",
            messages: {
                required: "You didn't confirm the password",
                minlength: 'Must have at least 6 characters!',
                equalTo: 'Passwords must match'
            }
        },
        'type': {
            required: true,
            messages: {
                required: "This field is required."
            }
        },
        'file': {
            required: true,
            messages: {
                required: "Please select an image"
            }
        },
        'questionnaire': {
            required: true,
            messages: {
                required: "Please select an image"
            }
        },
        'subscribe': {
            require_from_group: [1, ".phone-group"],
            messages: {
                require_from_group: "Выберите специализацию"
            }
        },
        'card': {
            required: true,
            messages: {
                required: "Enter card number"
            }
        },
        'expDate': {
            expDate: true,
            required: true,
            messages: {
                expDate: "Invalid Date",
                required: "Enter Date"
            }
        },
        'cvsCode': {
            required: true,
            messages: {
                required: "Enter cvs code"
            }
        },
        'address': {
            required: true,
            messages: {
                required: "Please enter billing address"
            }
        },
        'zipCode': {
            required: true,
            messages: {
                required: "Please enter zip code"
            }
        },
        'state': {
            required: true,
            messages: {
                required: "Please enter state"
            }
        },
        'city': {
            required: true,
            messages: {
                required: "Please enter city"
            }
        },
        'condition': {
            required: true,
            messages: {
                required: "Enter medical condition"
            }
        },
        'medicine': {
            required: true,
            messages: {
                required: "Enter medicine"
            }
        },
        'lastCard': {
            required: true,
            messages: {
                required: "Enter last 4 number on credit card"
            },
            minlength: 4,
            maxlength: 4
        },
        'recaptcha': {
            required: true,
            messages: {
                required: 'Check the checkbox'
            }
        }
    };

    for (var ruleName in rules) {
        $('[data-valid=' + ruleName + ']').each(function(){
            $(this).rules('add', rules[ruleName]);
        });
    }
}());

var dateValid = false,
    dateCurrent = new Date(),
    monthCurrent = dateCurrent.getMonth() + 1,
    yearCurrent = String(dateCurrent.getFullYear());
yearCurrent = yearCurrent[2] + yearCurrent[3];

$(window).on('load focusout', function() {
    checkValidExpDate('.exp-date');
});


// $(document).on('keyup', '.exp-date', function(){
//     checkValidExpDate($(this));
// });

function checkValidExpDate(el) {
    if (isOnPage('.exp-date')) {
        var expDate = $(el).val().split('/');
        var regexp = /\•+\/\•+/;
        if(regexp.test($(el).val())){
          dateValid = true;
        } else{
          ((Number(expDate[1]) > Number(yearCurrent)) || ((Number(expDate[1]) == Number(yearCurrent)) && ((Number(expDate[0]) >= Number(monthCurrent))))) ? dateValid = true : dateValid = false;
        }
    }

}

// custom rules
//-----------------------------------------------------------------------------------
$.validator.addMethod("email", function(value) {
    if (value == '') return true;
    var regexp = /[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    return regexp.test(value);
});

$.validator.addMethod("letters", function(value, element) {
    return this.optional(element) || /^[^1-9!@#\$%\^&\*\(\)\[\]:;,.?=+_<>`~\\\/"]+$/i.test(value);
});

$.validator.addMethod("digits", function(value, element) {
    return this.optional(element) || /^(\+?\d+)?\s*(\(\d+\))?[\s-]*([\d-]*)$/i.test(value);
});

$.validator.addMethod("valueNotEquals", function(value, element, arg){
    return arg != value;
}, "Value must not equal arg.");

$.validator.addMethod("expDate", function (value) {
    if (value == '') return true;
    var regexp = /((0[1-9]|1[012])[- /.]([0-9]{2}))|\•+\/\•+/;
    if (dateValid && regexp.test(value))
        return true
    else
        return false;
});


$.validator.addMethod( "require_from_group", function( value, element, options ) {
    var $fields = $( options[ 1 ], element.form ),
        $fieldsFirst = $fields.eq( 0 ),
        validator = $fieldsFirst.data( "valid_req_grp" ) ? $fieldsFirst.data( "valid_req_grp" ) : $.extend( {}, this ),
        isValid = $fields.filter( function() {
            return validator.elementValue( this );
        } ).length >= options[ 0 ];

    // Store the cloned validator for future validation
    $fieldsFirst.data( "valid_req_grp", validator );

    // If element isn't being validated, run each require_from_group field's validation rules
    if ( !$( element ).data( "being_validated" ) ) {
        $fields.data( "being_validated", true );
        $fields.each( function() {
            validator.element( this );
        } );
        $fields.data( "being_validated", false );
    }
    return isValid;
}, $.validator.format( "Please fill at least {0} of these fields." ) );