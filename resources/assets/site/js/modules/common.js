'use strict';
// fixed svg show
//-----------------------------------------------------------------------------
function fixedSvg() {
    var baseUrl = window.location.protocol + '//' + window.location.host + window.location.pathname + window.location.search;
    $('use').filter(function() {
        return ($(this).attr("xlink:href").indexOf("#") > -1);
    }).each(function() {
        $(this).attr("xlink:href", baseUrl + $(this).attr("xlink:href").split('/').slice(-1)[0]);
    });
}

fixedSvg();

// checking if element for page
//-----------------------------------------------------------------------------------
function isOnPage(selector) {
    return ($(selector).length) ? $(selector) : false;
}

$(document).on('opening', '.remodal', function () {
    $('.page-wrapper').addClass("blur");
});

$(document).ready(function(){
    $('input').each(function(){
        if($(this).val().length != 0){
            $(this).parent('.steps-label').addClass("focus-steps-input");
        }
    });
});

$('.js-steps-input').on('focus', function(){
    $(this).parent('.steps-label').addClass("focus-steps-input");
});

$('.js-steps-input').on('blur', function(){
    if($(this).val().length <= 0){
        $(this).parent('.steps-label').removeClass("focus-steps-input");
    }
});

$('.js-steps-textarea').on('focus', function(){
    $(this).parent('.steps-label').addClass("focus-steps-input");
    $(this).siblings('.steps-placeholder').css({opacity: 0});
});

$('.js-steps-textarea').on('blur', function(){
    if($(this).val().length <= 0){
        $(this).parent('.steps-label').removeClass("focus-steps-input");
        $(this).siblings('.steps-placeholder').css({opacity: 1});
    }
});

$('.mobile-button-menu').click(function(){
    if($(this).hasClass('mobile-button-menu-open')){
        $('.header-right').slideUp();
        $(this).removeClass("mobile-button-menu-open");
        setTimeout(function(){
            $('.header-right').removeClass("flex");
        },300);
    } else{
        $('.header-right').slideDown();
        $(this).addClass("mobile-button-menu-open");
        $('.header-right').addClass("flex");
    }
});

$(document).on('focus', '.js-steps-textarea', function(){
    $(this).parent('.steps-label').addClass("focus-steps-input");
    $(this).siblings('.steps-placeholder').css({opacity: 0});
});

$(document).on('blur', '.js-steps-textarea', function(){
    if($(this).val().length <= 0){
        $(this).parent('.steps-label').removeClass("focus-steps-input");
        $(this).siblings('.steps-placeholder').css({opacity: 1});
    }
});

$(document).on('click', '.mobile-button-menu-header', function(){
    if($(this).hasClass('footer-menu-open')){
        $(this).removeClass('footer-menu-open');
        $(this).siblings('.mobile-footer').stop().slideUp();
        $('footer, .page-body').removeClass('blur');
        $('body').css({overflow: 'auto'});
    } else{
        $(this).addClass('footer-menu-open');
        $(this).siblings('.mobile-footer').stop().slideDown();
        $('footer, .page-body').addClass('blur');
        $('body').css({overflow: 'hidden'});
    }
});

var $form = $('form');
$form.each(function () {

    var thisForm = $(this);
    thisForm.on('change', 'input, textarea', function (e) {
         e.preventDefault();
         if (thisForm.validate().checkForm()) {
             thisForm.find('button[type="submit"]').addClass('enable-btn');
         } else {
             thisForm.find('button[type="submit"]').removeClass('enable-btn');
         }
     });
});

$('.js-mobiscroll-wakeTime').mobiscroll().time({
    theme: 'allswell',
    display: 'center',
    steps: {
        minute: 5
    },
    headerText: 'Wake-time',
    timeFormat: 'hh-ii-A',
    defaultValue: mobiscroll.util.datetime.parseDate('yy-mm-dd-hh-ii-A', '1900-1-1-8-0-0-AM'),
    animate: 'flip',
    focusOnClose: false,
    onClose: function () {
        $(this).valid();
    }
});

$('.js-mobiscroll-bedTime').mobiscroll().time({
    theme: 'allswell',
    display: 'center',
    steps: {
        minute: 5
    },
    headerText: 'Bed-time',
    timeFormat: 'hh-ii-A',
    defaultValue: mobiscroll.util.datetime.parseDate('yy-mm-dd-hh-ii-A', '1900-1-1-22-0-0-PM'),
    animate: 'flip',
    focusOnClose: false,
    onClose: function () {
        $(this).valid();
    }
});

$('.js-mobiscroll-hour').mobiscroll().time({
    theme: 'allswell',
    display: 'center',
    timeFormat: 'hh',
    headerText: 'Time interval',
    defaultValue: new Date(new Date().setHours(2, 0, 0, 0)),
    animate: 'flip',
    focusOnClose: false,
    onClose: function () {
        $(this).valid();
    }
});

$('.js-mobiscroll-reminder').mobiscroll().time({
    theme: 'allswell',
    display: 'center',
    steps: {
        minute: 5
    },
    headerText: 'Reminder',
    timeFormat: 'hh-ii-A',
    defaultValue: mobiscroll.util.datetime.parseDate('yy-mm-dd-hh-ii-A', '1900-1-1-10-0-0-AM'),
    animate: 'flip',
    focusOnClose: true
});

var stopVideo = function (player) {
    var vidSrc = player.prop('src').replace('autoplay=1', 'autoplay=0');
    player.prop('src', vidSrc);
};

$(document).on('closed', '.modal-video', function () {
    var $current, player;
    $current = $(".modal-video");

    player = $current.find("iframe").get(0);
    stopVideo($(player));
});

$(document).on('opening', '.refer-modal', function(){
    $('body').css({position: 'fixed'});
});

$(document).on('closed', '.refer-modal', function(){
    $('body').css({position: 'relative'});
});

var phoneMask = {
    'alias': 'phone',
    showMaskOnFocus: true,
    showMaskOnHover: false
};

$(document).on('click', '.js-new-recipient', function(){
    $('[data-mask="phone"]').inputmask("phone", phoneMask);
});

$('[data-mask="phone"]').inputmask("phone", phoneMask);

$(document).on('click', '.js-steps-label-fullname', function(){
    $(this).css({display: 'none'});
    $('.js-steps-half-block').css({display: 'flex'});
    $('.js-steps-half-block').find('.steps-input[name="first_name"]').trigger("refresh").focus();
    $('.js-steps-half-block').find('.steps-input[name="last_name"]');
});

$(document).ready(function(){
    var $input = $('.js-steps-label-fullname').siblings('.js-steps-half-block').find('.steps-input');
    if($input.length != 0 && $input.val().length != 0){
        $('.js-steps-label-fullname').css({display: 'none'});
        $('.js-steps-half-block').css({display: 'flex'});
        $('.js-steps-half-block').find('.steps-input[name="name"]').trigger("refresh").focus();
        $('.js-steps-half-block').find('.steps-input[name="lastName"]');
    }
});

$(document).on('keypress change', 'input[name="name"], input[name="lastName"]', function(e){
    if (e.keyCode > 48 && e.keyCode < 57) {
        return false;
    }
});

$(document).on('keypress keydown', 'input, textarea', function(){
    // var $this = $(this);
    // setTimeout(function(){
    //     $this.parents('.js-validate').valid();
    // }, 100);
});
var urlImgLoad = '';
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader(),
            fileName = input.files[0].name;
        reader.onload = function (e) {
            urlImgLoad = e.target.result;
            $('.loaded-img').append('<div class="img-item"><img class="loaded-img" src="'+urlImgLoad+'" alt=""><div class="img-name-text">'+fileName+'</div><div class="remove js-remove-img"><img src="/assets/img/remove-icon.svg" alt=""></div></div>');
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$(document).on('change', '.js-submitfile', function(){
    readURL(this);
    $(this).closest('.load-img-block').addClass('is-active');
});
$(document).on('click', '.js-remove-img', function(e){
    e.preventDefault();
    $(this).closest('.load-img-block').removeClass('is-active');
    $(this).closest('.img-item').remove();
    $('.js-submitfile').val('');

});

function recaptchaInit() {
    $('.g-recaptcha').each(function(index, el) {
        var $recaptcha = $(this);
        var $field     = $recaptcha.closest('.el-form-field');
        var sitekey    = $(this).data('sitekey');

        grecaptcha.render(el, {
            'sitekey' : sitekey,
            callback : function() {
                recaptchaOnSuccess($field);
            }
        });
    });
};

function recaptchaOnSuccess($field) {
  $('.js-recaptcha-check').prop('checked', true);
  if ($('form').validate().checkForm()) {
    $('form').find('button[type="submit"]').addClass('enable-btn');
  }
};

// $('input, textarea').on('focus',function () {
//     setTimeout((function (el) {
//         var strLength = el.value.length;
//         var el = $(el);
//         return function () {
//             if (el.setSelectionRange !== undefined) {
//                 el.setSelectionRange(strLength, strLength);
//             } else {
//                 $(el).val(el.value);
//             }
//         }
//     }(this)), 5);
// });

$(window).scroll(function(){
    if ( $(window).scrollTop() >= 10 ){
        $('.header').addClass('header-scrolled');
    } else {
        $('.header').removeClass('header-scrolled');
    }
});

$(window).on('load', function(){
    if ( $(window).scrollTop() >= 10 ){
        $('.header').addClass('header-scrolled');
    } else {
        $('.header').removeClass('header-scrolled');
    }
});

$(document).on('focus', 'input, textarea', function(){
    moveCursorToEnd($(this));
});

function moveCursorToEnd(input) {
    var originalValue = input.val();
    input.val('');
    input.val(originalValue);
}

$(window).on('load', function(){
    if(isOnPage('.steps')){
        var scrollLeftPos = $('.steps ul .current').position().left;
        $('.steps ul').scrollLeft(scrollLeftPos - 20);
    }
});

$(window).on('load', function(){
    //$('.pre-loader').fadeOut();
});