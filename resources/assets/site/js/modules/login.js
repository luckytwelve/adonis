App.modules.set('login', {
    events: {
        'submit: form[name="form-login"]': 'submitForm'
    },


    init: function() {

    },


    submitForm: function(e){
        e.preventDefault();
        var _this = $(this);
        App.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: _this.attr('action'),
            type: 'post',
            data: _this.serialize(),
            success: function(response){
                if (response.validatorErrors) {

                    App.backendValidation(
                        $('[data-module="login"]'),
                        response.validatorErrors
                    );
                }
            }
        });
    },
});